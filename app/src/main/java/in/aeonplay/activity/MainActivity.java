package in.aeonplay.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.fragment.BaseFragment;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.fragment.EpisodeFragment;
import in.aeonplay.fragment.HomeFragment;
import in.aeonplay.fragment.MoreFragment;
import in.aeonplay.fragment.MoviesFragment;
import in.aeonplay.fragment.MyPlanFragment;
import in.aeonplay.fragment.NewsFragment;
import in.aeonplay.fragment.PlanFragment;
import in.aeonplay.fragment.PremiumFragment;
import in.aeonplay.fragment.SearchFragment;
import in.aeonplay.fragment.SettingsFragment;
import in.aeonplay.fragment.TVShowsFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Profile.ProfileModel;
import in.aeonplay.model.SubscriptionModel.ActiveSubscription;
import in.aeonplay.model.SubscriptionModel.ContentAccess;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import in.aeonplay.slidingmenu.Item;
import in.aeonplay.slidingmenu.ItemClickListener;
import in.aeonplay.slidingmenu.Section;
import in.aeonplay.slidingmenu.SectionedExpandableLayoutHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MainActivity extends MasterActivity implements NavigationBarView.OnItemSelectedListener {

    private BottomNavigationView mBottomNavigationView;
    private RecyclerView mDrawerMenuList;
    public static DrawerLayout mDrawerLayout;
    private TextView mProfileName, mProfileMobile;
    private Toolbar mToolbar;
    public static TextView mToolbarTitle;
    private FrameLayout mFrameLayout;
    int bottomNavigationHeight = 0;
    private LinearLayout mLinearProfile;
    private ImageView mBottomNavigationFree;
    private Button btnPlanUpgrade, btnMyPlan, btnReadNewspaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getExtras() != null) {
            addFragment(new MyPlanFragment());

        } else {
            addFragment(new HomeFragment());
        }

        Init();

        setUpNavigationDrawer();
        setUpBottomNavigation();
        getProfile();
        getSubscriptionList();

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {

                    @Override
                    public void onBackStackChanged() {
                        Fragment f = getSupportFragmentManager()
                                .findFragmentById(R.id.fragment_container);

                        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            finish();
                        } else {
                            if (f != null) {
                                updateToolbarTitle(f);
                            }
                        }

                    }
                });

        checkForAppUpdate();
    }

    private void updateToolbarTitle(Fragment fragment) {
        String fragClassName = fragment.getClass().getName();
        if (fragClassName.equals(HomeFragment.class.getName())) {
            updateBottomNavigation(0);
            showToolbar();

        } else if (fragClassName.equals(MoviesFragment.class.getName())) {
            updateBottomNavigation(1);
            showToolbar();

        } else if (fragClassName.equals(PremiumFragment.class.getName())) {
            updateBottomNavigation(2);
            showToolbar();

        } else if (fragClassName.equals(TVShowsFragment.class.getName())) {
            updateBottomNavigation(3);
            showToolbar();

        } else if (fragClassName.equals(SearchFragment.class.getName())) {
            updateBottomNavigation(4);
            hideToolbar();

        } else if (fragClassName.equals(DetailFragment.class.getName())) {
            hideToolbar();

        } else if (fragClassName.equals(SettingsFragment.class.getName())) {
            hideToolbar();

        } else if (fragClassName.equals(EpisodeFragment.class.getName())) {
            hideToolbar();

        } else if (fragClassName.equals(MoreFragment.class.getName())) {
            showToolbar();

        } else if (fragClassName.equals(MyPlanFragment.class.getName())) {
            showToolbar();

        } else if (fragClassName.equals(PlanFragment.class.getName())) {
            showToolbar();
        }
    }

    private void setUpNavigationDrawer() {
        String text = "<big><font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + SharePreferenceManager.getUserData().getFirstName() + "</font></big><br>" +
                "<small><font color=" + getResources().getColor(R.color.colorGray) + "> " + SharePreferenceManager.getUserData().getLastName() + "</font></small>";
        mProfileName.setText(Html.fromHtml(text));
        mProfileMobile.setText(SharePreferenceManager.getUserData().getMobileNo());

        mLinearProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                addFragment(new SettingsFragment());
            }
        });

        btnPlanUpgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                addFragment(new PlanFragment());
            }
        });

        btnMyPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                addFragment(new MyPlanFragment());
            }
        });

        btnReadNewspaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawers();
                addFragment(new NewsFragment());
            }
        });

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mDrawerMenuList.getLayoutParams();
        layoutParams.topMargin = getStatusBarHeight() / 2;
        layoutParams.bottomMargin = getNavigationBarHeight();
        mDrawerMenuList.setLayoutParams(layoutParams);

        SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(MainActivity.this,
                mDrawerMenuList, new ItemClickListener() {
            @Override
            public void itemClicked(Item item) {
                mDrawerLayout.closeDrawers();
                showMessageToUser("Coming Soon");
            }

            @Override
            public void itemClicked(Section section) {
                mDrawerLayout.closeDrawers();
                showMessageToUser("Coming Soon");
            }
        });

        //random data
        ArrayList<Item> arrayList = new ArrayList<>();
        arrayList = new ArrayList<>();
        arrayList.add(new Item("Search Course", 0));
        arrayList.add(new Item("Boards", 2));
        arrayList.add(new Item("Primary Education", 3));
        arrayList.add(new Item("K to XII", 4));
        arrayList.add(new Item("Universities & Courses", 5));
        arrayList.add(new Item("Professional", 6));
        arrayList.add(new Item("Short Duration Pro", 7));
        arrayList.add(new Item("Advanced Studies", 8));
        sectionedExpandableLayoutHelper.addSection("Education", R.drawable.ic_action_education, true, arrayList);

        arrayList = new ArrayList<>();
        arrayList.add(new Item("Specialization", 0));
        arrayList.add(new Item("Hospitals / Locations", 1));
        arrayList.add(new Item("Consultation", 2));
        arrayList.add(new Item("Pathology", 3));
        arrayList.add(new Item("Fitness", 4));
        arrayList.add(new Item("Diet", 5));
        arrayList.add(new Item("Patanjali YOGA", 6));
        arrayList.add(new Item("Ayurved & Lifestyle", 7));
        arrayList.add(new Item("Useful Links", 8));
        sectionedExpandableLayoutHelper.addSection("HealthCare", R.drawable.ic_action_healthcare, false, arrayList);

        arrayList = new ArrayList<>();
        arrayList.add(new Item("Search Product", 0));
        arrayList.add(new Item("Apparels", 1));
        arrayList.add(new Item("Lifestyle", 2));
        arrayList.add(new Item("Brands", 3));
        arrayList.add(new Item("Home", 4));
        arrayList.add(new Item("Automobile", 5));
        sectionedExpandableLayoutHelper.addSection("Shopping", R.drawable.ic_action_shopping, false, arrayList);

        arrayList = new ArrayList<>();
        arrayList.add(new Item("Search Service", 0));
        arrayList.add(new Item("Lifestyle", 1));
        arrayList.add(new Item("Home", 2));
        arrayList.add(new Item("Professional Services", 3));
        arrayList.add(new Item("Travel", 4));
        arrayList.add(new Item("Maintenance & Operations", 5));
        sectionedExpandableLayoutHelper.addSection("Utilities", R.drawable.ic_action_utility, false, arrayList);

        sectionedExpandableLayoutHelper.notifyDataSetChanged();
    }

    private void Init() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbarTitle = findViewById(R.id.toolbarTitle);
        mToolbarTitle.setTypeface(getLogoFont());

        mBottomNavigationFree = findViewById(R.id.bottomNavigationFree);
        mBottomNavigationView = findViewById(R.id.bottomNavigationView);
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mDrawerMenuList = findViewById(R.id.mDrawerMenuList);
        mDrawerMenuList.setNestedScrollingEnabled(false);
        mDrawerMenuList.setItemAnimator(new DefaultItemAnimator());

        mLinearProfile = findViewById(R.id.linearProfile);
        btnPlanUpgrade = findViewById(R.id.btnPlanUpgrade);
        btnMyPlan = findViewById(R.id.btnMyPlan);
        btnReadNewspaper = findViewById(R.id.btnReadNewspaper);

        mProfileName = findViewById(R.id.profile_name);
        mProfileMobile = findViewById(R.id.profile_mobile);

        mFrameLayout = findViewById(R.id.fragment_container);
//        mBottomNavigationView.post(new Runnable() {
//            @Override
//            public void run() {
//                bottomNavigationHeight = (int) mBottomNavigationView.getMeasuredHeight();
//                mFrameLayout.setPadding(0, 0, 0, bottomNavigationHeight);
//            }
//        });

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mDrawerMenuList.getContext(),
                LinearLayoutManager.VERTICAL);
        mDrawerMenuList.addItemDecoration(dividerItemDecoration);
    }

    public boolean addFragment(final Fragment fragment) {
        if (fragment != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentTransaction transaction = getSupportFragmentManager()
                            .beginTransaction();
                    transaction.setCustomAnimations(
                            R.anim.slide_in,  // enter
                            R.anim.slide_out  // exit
                    );
                    transaction.add(R.id.fragment_container, fragment);
                    transaction.addToBackStack(fragment.getClass().getName());
                    transaction.commitAllowingStateLoss();
                }
            }, 200);

            return true;
        }
        return false;
    }

    public void removeFragment(final Fragment fragments) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getSupportFragmentManager()
                        .beginTransaction();
                transaction.setCustomAnimations(
                        R.anim.slide_in,  // enter
                        R.anim.slide_out  // exit
                );
                transaction.detach(fragments);
                transaction.remove(fragments);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        }, 200);
    }


    private void setUpBottomNavigation() {
        mBottomNavigationView.setOnItemSelectedListener(this);
    }

    public void updateBottomNavigation(int position) {
        mBottomNavigationView.getMenu().getItem(position).setChecked(true);

        if (mBottomNavigationView.getMenu().getItem(2).isChecked()) {
            mBottomNavigationFree.setImageResource(R.drawable.ic_action_center_aeon_on);
        } else {
            mBottomNavigationFree.setImageResource(R.drawable.ic_action_center_aeon_off);
        }
    }

    public void showToolbar() {
        mToolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        mToolbar.setVisibility(View.GONE);
    }

    public void updateBottomNavigationDisable(int position) {
        mBottomNavigationView.getMenu().getItem(position).setCheckable(false);
    }

    public void updateBottomNavigationDisableAll() {
        mBottomNavigationView.getMenu().setGroupCheckable(0, false, true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_item_1:
                if (isExists(HomeFragment.class))
                    fragment = new HomeFragment();
                else
                    return false;
                break;

            case R.id.nav_item_2:
                if (isExists(MoviesFragment.class)) {
                    fragment = new MoviesFragment();
                } else
                    return false;
                break;

            case R.id.nav_item_3:
                if (isExists(PremiumFragment.class))
                    fragment = new PremiumFragment();
                else
                    return false;
                break;

            case R.id.nav_item_4:
                if (isExists(TVShowsFragment.class)) {
                    fragment = new TVShowsFragment();
                } else
                    return false;
                break;

            case R.id.nav_item_5:
                if (isExists(SearchFragment.class))
                    fragment = new SearchFragment();
                else
                    return false;
                break;
        }

        return addFragment(fragment);
    }

    private boolean isExists(Class aClass) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        return !aClass.isInstance(fragment);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return onBack();
        }

        return super.onKeyDown(keyCode, event);
    }

    public boolean onBack() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            BaseFragment fragment = (BaseFragment) fm.findFragmentById(R.id.fragment_container);
            fragment = (BaseFragment) fm.findFragmentById(R.id.fragment_container);
            fragment.onBack();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private void getProfile() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUserProfile(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData().getStatus() == 1) {
                        SharePreferenceManager.setUserData(profileModel.getData());

                        String text = "<big><font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + profileModel.getData().getFirstName() + "</font></big><br>" +
                                "<small><font color=" + getResources().getColor(R.color.colorGray) + "> " + profileModel.getData().getLastName() + "</font></small>";
                        mProfileName.setText(Html.fromHtml(text));
                        mProfileMobile.setText(profileModel.getData().getMobileNo() + "");
                    }

                    if (profileModel.getData().getMobileNetworkOperator() == null &&
                            profileModel.getData().getMnc() == null &&
                            profileModel.getData().getMcc() == null &&
                            profileModel.getData().getMobileNumberRegisteredLocation() == null) {
                        getUpdateOperator();
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getSubscriptionList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSubscriptionHistory(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData() != null) {

                        // Check for active subscription ....
                        activeSubscriptionList = new ArrayList<>();
                        if (profileModel.getData().getActiveSubscription() != null) {
                            activeSubscriptionList.addAll(profileModel.getData().getActiveSubscription());
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getUpdateOperator() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getUpdtOperator(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final ProfileModel profileModel = gson.fromJson(reader, ProfileModel.class);

                    if (profileModel.getData().getStatus() == 1) {
                        SharePreferenceManager.setUserData(profileModel.getData());

                        String text = "<big><font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + profileModel.getData().getFirstName() + "</font></big><br>" +
                                "<small><font color=" + getResources().getColor(R.color.colorGray) + "> " + profileModel.getData().getLastName() + "</font></small>";
                        mProfileName.setText(Html.fromHtml(text));
                        mProfileMobile.setText(profileModel.getData().getMobileNo() + "");
                    }

//                    checkActiveSubscription = profileModel.getData().getActiveSubscription();
//                    checkSubscriptionHistoryList = profileModel.getData().getSubscriptionHistory();

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    public void requestDeepLink(CommanDataList commanDataModel) {
        if (commanDataModel.getProvider().equalsIgnoreCase("sonyliv")) {

            if (activeSubscriptionList.size() > 0) {
                for (ActiveSubscription activeSubscription : activeSubscriptionList) {
                    for (ContentAccess contentAccess : activeSubscription.getSubscriptionPlan().getContentAccess()) {
                        if (commanDataModel.getProvider().equalsIgnoreCase(contentAccess.getTitle())) {

                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    launchAppUsingSSO(commanDataModel.getDeeplink(), commanDataModel.getExtra().getApkPackageId());
                                }

                            } catch (ActivityNotFoundException | NullPointerException | SecurityException e) {
                                openStore(commanDataModel.getExtra().getApkPackageId());
                            }

                        } else {
                            addFragment(new PlanFragment());
                        }
                    }
                }

            } else {
                addFragment(new PlanFragment());
            }

        } else if (commanDataModel.getProvider().equalsIgnoreCase("aeonplay")) {
            Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
            intent.putExtra(Constants.DATA, commanDataModel);
            intent.putExtra(Constants.TRAILER, commanDataModel.getTraileHostUrl());
            startActivity(intent);

        } else {
            Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
            intent.putExtra(Constants.DATA, commanDataModel);
            startActivity(intent);
        }
    }

}