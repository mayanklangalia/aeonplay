package in.aeonplay.activity;

import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
import static android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
import static android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.android.exoplayer2.BuildConfig;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManagerProvider;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.ext.ima.ImaServerSideAdInsertionMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.TimeBar;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.DemoUtil;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.PlaybackModel.PlaybackModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PlayerActivity extends MasterActivity {

    private String TAG = PlayerActivity.class.getSimpleName();
    private PlayerView playerView;
    private ProgressBar progressBar;
    private ExoPlayer exoPlayer;
    private DefaultTrackSelector mTrackSelector;
    private LinearLayout linearBack, linearBottom, linearLock;
    private ImageButton imgLock;
    private Bundle bundle;
    private CommanDataList commanDataList;
    private TextView txtMediaTitle, txtLockTitle;
    private String assetType = "";
    private AdsLoader clientSideAdsLoader;
    private ImaServerSideAdInsertionMediaSource.AdsLoader serverSideAdsLoader;
    private ImaServerSideAdInsertionMediaSource.AdsLoader.State serverSideAdsLoaderState;
    private DataSource.Factory dataSourceFactory;
    private String trailerUrl;
    private RelativeLayout relativeTimer;
    private RewardedInterstitialAd rewardedInterstitialAd = null;

    private ArrayList<Long> list = new ArrayList<>();
    private int index = 0;
    private DefaultTimeBar defaultTimeBar;
    boolean isLock = false;

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        dataSourceFactory = new DefaultDataSourceFactory(
                this, Util.getUserAgent(this, getString(R.string.app_name)));

        hideToolbarStatus();

        playerView = findViewById(R.id.player_view);
        progressBar = findViewById(R.id.player_loader);

        defaultTimeBar = playerView.findViewById(R.id.exo_progress);
        txtMediaTitle = playerView.findViewById(R.id.txtMediaTitle);
        relativeTimer = playerView.findViewById(R.id.relativeTimer);
        imgLock = playerView.findViewById(R.id.imgLock);
        txtLockTitle = playerView.findViewById(R.id.txtLockTitle);
        linearBottom = playerView.findViewById(R.id.linearBottom);
        linearLock = playerView.findViewById(R.id.linearLock);
        linearLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLock) {
                    imgLock.setImageResource(R.drawable.ic_action_lock);
                } else {
                    imgLock.setImageResource(R.drawable.ic_action_unlock);
                }
                isLock = !isLock;
                lockScreen(isLock);
            }
        });

        linearBack = playerView.findViewById(R.id.linearBack);
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bundle = getIntent().getExtras();

        if (bundle != null) {
            commanDataList = bundle.getParcelable(Constants.DATA);
            trailerUrl = bundle.getString(Constants.TRAILER);
            txtMediaTitle.setText(commanDataList.getTitle());

            assetType = commanDataList.getContentType().equalsIgnoreCase("episode") ? "original" : "movie";

//            if (commanDataList.getProvider().equalsIgnoreCase("erosnow")) {
//                requestPlayback(assetType, commanDataList.getContentId());
//            } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                configurePlayer("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            }
//            }
        }
    }

    public void lockScreen(boolean lock) {
        linearBottom.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        txtLockTitle.setText(lock == true ? "Unlock" : "Lock");
        defaultTimeBar.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
        relativeTimer.setVisibility(lock == true ? View.INVISIBLE : View.VISIBLE);
    }

    public static boolean useExtensionRenderers() {
        return BuildConfig.DEBUG;
    }

    public static RenderersFactory buildRenderersFactory(
            Context context, boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(context.getApplicationContext())
                .setExtensionRendererMode(extensionRendererMode);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void configurePlayer(String path) {
        mTrackSelector = new DefaultTrackSelector(this);
        mTrackSelector.setParameters(new DefaultTrackSelector.ParametersBuilder()
                .setRendererDisabled(C.TRACK_TYPE_VIDEO, false)
                .build());

        boolean preferExtensionDecoders = false;
        RenderersFactory renderersFactory = buildRenderersFactory(/* context= */ this, preferExtensionDecoders);

        exoPlayer = new ExoPlayer.Builder(this, renderersFactory)
                .setMediaSourceFactory(createMediaSourceFactory())
                .setTrackSelector(mTrackSelector)
                .setReleaseTimeoutMs(2000)
                .build();

        playerView.setPlayer(exoPlayer);
        playerView.setKeepScreenOn(true);

        defaultTimeBar.addListener(new TimeBar.OnScrubListener() {

            @Override
            public void onScrubStart(TimeBar timeBar, long position) {

            }

            @Override
            public void onScrubMove(TimeBar timeBar, long position) {
                exoPlayer.seekTo(position);
                if (list.size() > 0) {
                    if (position == list.get(index)) {
                        initAd();
                        index++;
                    }
                }
            }

            @Override
            public void onScrubStop(TimeBar timeBar, long position, boolean canceled) {

            }
        });

        exoPlayer.addListener(new Player.Listener() {
            @Override
            public void onPlaybackStateChanged(int playbackState) {
                Player.Listener.super.onPlaybackStateChanged(playbackState);

                if (playbackState == ExoPlayer.STATE_READY) {
                    progressBar.setVisibility(View.INVISIBLE);

//                    showAdvertisementEvery5Min(exoPlayer.getDuration());
                    showAdvertisementTotalDurationDividedBy10(exoPlayer.getDuration());

                } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar.setVisibility(View.VISIBLE);

                } else if (playbackState == ExoPlayer.STATE_IDLE) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPlayerError(PlaybackException error) {
                Player.Listener.super.onPlayerError(error);

                StringWriter errors = new StringWriter();
                error.printStackTrace(new PrintWriter(errors));
                String errorLog = errors.toString();
                Log.e("Error", errorLog);
            }
        });

        prepareMediaForPlaying(path);
        setPlayPause(true);


    }

    private void showAdvertisementTotalDurationDividedBy10(long totalDuration) {
        if (totalDuration != 0) {
            long adsLong[] = new long[10];
            boolean adsBoolean[] = new boolean[10];

            for (int i = 1; i <= 10; ++i) {
                list.add((totalDuration / 10) * i);
                adsLong[i - 1] = ((totalDuration / 10) * i);
                adsBoolean[i - 1] = false;
            }

            playerView.setExtraAdGroupMarkers(
                    adsLong, adsBoolean);

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (index < list.size()) {
                            if ((int) exoPlayer.getCurrentPosition() >= list.get(index)) {
                                initAd();
                                index++;
                            }
                        }

                        handler.postDelayed(runnable, 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.post(runnable);
        }
    }

    private void showAdvertisementEvery5Min(long totalDuration) {
        if (totalDuration != 0) {
            long totalCalculatedDuration = 0;
            long delay = 5 * 60 * 1000;

            long adsLong[] = new long[(int) Math.ceil(totalDuration / (delay * 1.0f))];
            boolean adsBoolean[] = new boolean[adsLong.length];

            int i = 0;
            while (totalCalculatedDuration < totalDuration) {
                list.add(delay);
                adsLong[i] = delay;
                adsBoolean[i] = false;
                i++;
                totalCalculatedDuration += delay;
            }

            playerView.setExtraAdGroupMarkers(
                    adsLong, adsBoolean);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    initAd();
                }
            });
        }
    }

    private void prepareMediaForPlaying(String mediaSourceUri) {
        if (mediaSourceUri != null) {
            Uri uri = Uri.parse(mediaSourceUri);
//            Uri adsUri = Uri.parse("https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=");

            MediaItem media = MediaItem.fromUri(uri);
            exoPlayer.setMediaItem(media);
//            MediaItem mediaItem =
//                    new MediaItem.Builder()
//                            .setUri(uri)
//                            .setAdsConfiguration(new MediaItem.AdsConfiguration.Builder(adsUri)
//                                    .setAdsId(adsUri).build())
//                            .build();

//            exoPlayer.setMediaItem(mediaItem);

        } else {
            showMessageToUser("Trailer not found!");
        }
    }


    private MediaSource.Factory createMediaSourceFactory() {
        DefaultDrmSessionManagerProvider drmSessionManagerProvider =
                new DefaultDrmSessionManagerProvider();
        drmSessionManagerProvider.setDrmHttpDataSourceFactory(
                DemoUtil.getHttpDataSourceFactory(/* context= */ this));
        ImaServerSideAdInsertionMediaSource.AdsLoader.Builder serverSideAdLoaderBuilder =
                new ImaServerSideAdInsertionMediaSource.AdsLoader.Builder(/* context= */ this, playerView);
        if (serverSideAdsLoaderState != null) {
            serverSideAdLoaderBuilder.setAdsLoaderState(serverSideAdsLoaderState);
        }
        serverSideAdsLoader = serverSideAdLoaderBuilder.build();
        ImaServerSideAdInsertionMediaSource.Factory imaServerSideAdInsertionMediaSourceFactory =
                new ImaServerSideAdInsertionMediaSource.Factory(
                        serverSideAdsLoader,
                        new DefaultMediaSourceFactory(/* context= */ this)
                                .setDataSourceFactory(dataSourceFactory));
        return new DefaultMediaSourceFactory(/* context= */ this)
                .setDataSourceFactory(dataSourceFactory)
                .setDrmSessionManagerProvider(drmSessionManagerProvider)
                .setLocalAdInsertionComponents(
                        this::getClientSideAdsLoader, /* adViewProvider= */ playerView)
                .setServerSideAdInsertionMediaSourceFactory(imaServerSideAdInsertionMediaSourceFactory);
    }

    private AdsLoader getClientSideAdsLoader(MediaItem.AdsConfiguration adsConfiguration) {
        // The ads loader is reused for multiple playbacks, so that ad playback can resume.
        if (clientSideAdsLoader == null) {
            clientSideAdsLoader = new ImaAdsLoader.Builder(/* context= */ this).build();
        }
        clientSideAdsLoader.setPlayer(exoPlayer);
        return clientSideAdsLoader;
    }

    private void setPlayPause(boolean play) {
        exoPlayer.setPlayWhenReady(play);
        exoPlayer.getPlaybackState();
        exoPlayer.prepare();
        exoPlayer.play();
    }

    public void hideToolbarStatus() {
        getWindow()
                .getDecorView()
                .setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LOW_PROFILE |
                                View.SYSTEM_UI_FLAG_FULLSCREEN |
                                SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
                                SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        getWindow().setFlags(FLAG_FULLSCREEN, FLAG_FULLSCREEN);
        getWindow().addFlags(FLAG_FULLSCREEN | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
            playerView.setPlayer(null);
            mTrackSelector = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        exoPlayer.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        exoPlayer.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exoPlayer.release();
    }

    @Override
    public void onBackPressed() {
        if (isLock) return;
        finish();
    }

    private void requestPlayback(String assetType, String assetID) {
        Log.d(TAG, "requestPlayback: " + assetType + " | " + assetID);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getPlaybackInfo(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), assetType, assetID);

        APIClient.callAPI(this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final PlaybackModel playbackModel = gson.fromJson(reader, PlaybackModel.class);

                    if (playbackModel.getData() != null) {
                        Log.d(TAG, "requestPlayback: " + playbackModel.getData().getPlayback().getUrl());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            configurePlayer(playbackModel.getData().getPlayback().getUrl());
                        }

                    } else {
                        showMessageToUser("Data not found!");
                        showErrorDialog(PlayerActivity.this,
                                loginCall.request().url().toString(),
                                Constants.EROS_PARTNER_PLAYBACK + "assetId=" + assetID + "&assetType=" + assetType);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                Log.d(TAG, "requestPlayback: " + error + " " + responseCode + " = " +
                        assetType + " - " + assetID);
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                Log.d(TAG, error);
                showMessageToUser(error);
            }
        });
    }

    private void initAd() {
        if (rewardedInterstitialAd != null) return;
        MobileAds.initialize(this);
        RewardedInterstitialAd.load(this, getString(R.string.rewarded_ads),
                new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull RewardedInterstitialAd p0) {
                        rewardedInterstitialAd = p0;
                        rewardedInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {

                            @Override
                            public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                                Log.d(TAG, "onAdFailedToShowFullScreenContent " + adError.getMessage());
                            }

                            @Override
                            public void onAdShowedFullScreenContent() {
                                Log.d(TAG, "onAdShowedFullScreenContent");
                            }

                            @Override
                            public void onAdDismissedFullScreenContent() {
                                Log.d(TAG, "onAdDismissedFullScreenContent");
                                setPlayPause(true);
                                rewardedInterstitialAd = null;
                            }
                        });

                        rewardedInterstitialAd.show(PlayerActivity.this, rewardItem -> {
                        });
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        Log.d(TAG, "onAdFailedToLoad " + loadAdError.getMessage());
                        rewardedInterstitialAd = null;
                    }
                });
    }
}

