package in.aeonplay.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.EncrptionDecrption;
import in.aeonplay.fragment.PlanFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.SubscriptionModel.ActiveSubscription;
import in.aeonplay.model.SubscriptionModel.ContentAccess;
import in.aeonplay.preferences.SharePreferenceManager;

public class MasterActivity extends AppCompatActivity {

    public static List<ActiveSubscription> activeSubscriptionList = new ArrayList<>();
    private final int RC_APP_UPDATE = 99;
    public MasterActivity masterActivity;
    // For App Update
    public AppUpdateManager appUpdateManager;
    private final String TAG = MasterActivity.class.getSimpleName();
    private AppUpdateInfo appUpdateInfo = null;

    public static String checkStringIsNull(String value) {
        if (value == null)
            return "";
        else if (value.equals("null"))
            return "";
        else
            return value;
    }

    public Typeface getNormalFont() {
        return Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
    }

    public Typeface getLogoFont() {
        return Typeface.createFromAsset(getAssets(), "Gothic-Bold.TTF");
    }

    public Typeface getDarkFont() {
        return Typeface.createFromAsset(getAssets(), "Roboto-Black.ttf");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        masterActivity = this;

        if (Build.VERSION.SDK_INT >= 30) {
            getWindow().setStatusBarColor(getColor(R.color.colorStatusBar));
            WindowCompat.setDecorFitsSystemWindows(getWindow(), false);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorStatusBar, this.getTheme()));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorStatusBar));
        }

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.colorStatusBar));

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        Log.d(TAG, "AccessToken: " + SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                SharePreferenceManager.getString(Constants.TOKEN_ACCESS));
    }

    public String getToolbarTitle(String name) {
        return name;
    }

    public void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public String getDecodedJWT(String jwt) {
        String result = "";

        String[] parts = jwt.split("[.]");
        try {
            int index = 0;
            for (String part : parts) {
                if (index >= 2)
                    break;

                index++;
                byte[] partAsBytes = part.getBytes(StandardCharsets.UTF_8);
                String decodedPart = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    decodedPart = new String(Base64.getUrlDecoder().decode(partAsBytes), StandardCharsets.UTF_8);
                }

                result += decodedPart;
            }
        } catch (Exception e) {
            throw new RuntimeException("Couldnt decode jwt", e);
        }

        return result;
    }

    public void showMessageToUser(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getToolbarHeight() {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        return actionBarHeight;
    }

    public int getNavigationBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getBottomNavigationBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("design_bottom_navigation_item_min_width", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


//    public void sendLogOnServer(Exception e) {
//        FirebaseCrashlytics.getInstance().setUserId(UserPreferences.getUserData(getApplicationContext()).getSubscriberId());
//        FirebaseCrashlytics.getInstance().setCustomKey("USER_EMAIL", UserPreferences.getUserData(getApplicationContext()).getEmail());
//        FirebaseCrashlytics.getInstance().recordException(e);
//    }

//    public String getFcmToken() {
//        FirebaseMessaging.getInstance().getToken()
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.d("FirebaseApp Token error: ", e.getMessage().toString());
//                    }
//                }).addOnCompleteListener(new OnCompleteListener<String>() {
//            @Override
//            public void onComplete(@NonNull Task<String> task) {
//                if (!task.isSuccessful()) {
//                    Log.d("TAG", "Fetching FCM registration token failed", task.getException());
//                    return;
//                }
//
//                fcmToken = task.getResult();
//                saveToken("fcm_token", fcmToken);
//                Log.d("FirebaseApp Token: ", fcmToken);
//            }
//        });
//
//        return fcmToken;
//    }

    public String getDeviceID() {
        return Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getDeviceName() {
        return Build.BRAND + " - " + Build.MODEL;
    }


    // FCM Token Preference
    public void saveFCMToken(String key, String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String loadFCMToken() {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this);
        return sp.getString("fcm_token", null);
    }

    public boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void showNoInternetDialog() {
        Toast.makeText(this, "Internet Connection", Toast.LENGTH_SHORT).show();
    }

    public void noActiveSubscription(MainActivity context, String upgrade) {
        final Dialog dialog = new Dialog(MasterActivity.this, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_comman);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.show();

        TextView dialogTitle = dialog.findViewById(R.id.dialogTitle);
        TextView dialogInfo = dialog.findViewById(R.id.dialogInfo);
        Button dialogCancel = dialog.findViewById(R.id.dialogCancel);
        Button dialogOk = dialog.findViewById(R.id.dialogOk);


        if (upgrade.equalsIgnoreCase("UPGRADE")) {
            dialogTitle.setText("Already subscribed!");
            dialogInfo.setText("You have already subscribed premium plan");
            dialogOk.setText("Okay");

        } else {
            dialogTitle.setText("No Active Subscription!");
            dialogInfo.setText("You don't have an active premium subscription. " +
                    "Visit the 'Offers' section to subscribe to one of our packs");
            dialogOk.setText("Yes");
        }

        dialogCancel.setText("Later");
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (!upgrade.equalsIgnoreCase("UPGRADE"))
                    context.addFragment(new PlanFragment());
            }
        });
    }

    public void showErrorDialog(FragmentActivity context, String requestURL, String requestPURL) {
        final Dialog dialog = new Dialog(context, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.show();

        TextView dialogInfo = dialog.findViewById(R.id.dialogInfo);
        TextView dialogPInfo = dialog.findViewById(R.id.dialogPInfo);
        Button dialogShare = dialog.findViewById(R.id.dialogShare);
        Button dialogCancel = dialog.findViewById(R.id.dialogCancel);

        dialogInfo.setText("");
        dialogPInfo.setText(requestPURL);
        dialogShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, requestURL + "\n\n" + requestPURL);
                sendIntent.setType("text/*");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });

        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void openStore(String apkPackageId) {
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + apkPackageId)));
    }

    public static String titleCase(String stringToConvert) {
        if (TextUtils.isEmpty(stringToConvert)) {
            return "";
        }
        return Character.toUpperCase(stringToConvert.charAt(0)) +
                stringToConvert.substring(1).toLowerCase();
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM, yyyy h:mma";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String changeDateFormatOnly(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM, yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
//
//    public void launchApp(String deeplink, String packageID) {
//        try {
//
//            if (deeplink.equalsIgnoreCase("")) {
//                Toast.makeText(MasterActivity.this, "Content Not Found", Toast.LENGTH_LONG).show();
//
//            } else {
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(deeplink));
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                if (intent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(intent);
//                } else {
//                    openStore(packageID);
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
////        Intent sonyLiv = new Intent();
////        sonyLiv.setAction(Intent.ACTION_VIEW);
////        sonyLiv.setData(Uri.parse(deeplink));
////        startActivity(sonyLiv);
//    }
//

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void launchAppUsingSSO(String deeplink, String packageID) {
        try {
            Log.d("SSO_mobile: ", SharePreferenceManager.getUserData().getMobileNo());
            EncrptionDecrption encrptionDecrption = new EncrptionDecrption();
            String paramValue = SharePreferenceManager.getUserData().getMobileNo() + "|" +
                    Instant.now().toEpochMilli() + 5 * 60 * 1000;
            String token = EncrptionDecrption.encrypt(paramValue, EncrptionDecrption.aesKey, EncrptionDecrption.iv);
            String decryptValue = EncrptionDecrption.decrypt(token, EncrptionDecrption.aesKey, EncrptionDecrption.iv);
            String rootURL = deeplink + "?source=aeoncom&loginType=sso&token=" + token;
            Log.d("SSO_aeoncom: ", rootURL);
            Log.d("SSO_param: ", paramValue);

            try {
                Intent sonyLiv = new Intent();
                sonyLiv.setAction(Intent.ACTION_VIEW);
                sonyLiv.setData(Uri.parse(rootURL));
                startActivity(sonyLiv);

            } catch (ActivityNotFoundException | NullPointerException | SecurityException e) {
                openStore(packageID);
            }

        } catch (InvalidKeyException e) {
            Log.d("SSO_IKE: ", e.getMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.d("SSO_UEE: ", e.getMessage());
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            Log.d("SSO_IAPE: ", e.getMessage());
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            Log.d("SSO_IBSE: ", e.getMessage());
            e.printStackTrace();
        } catch (BadPaddingException e) {
            Log.d("SSO_BPE: ", e.getMessage());
            e.printStackTrace();
        }
    }

    // In App Update ...
    public void checkForAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(masterActivity);
        appUpdateManager.registerListener(installStateUpdatedListener);

        // Returns an intent object that you use to check for an update.
        com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks whether the platform allows the specified type of update,
        // and checks the update priority.
        Log.d(TAG, "AppUpdate: Checking for app update");
        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.d(TAG, "AppUpdate: App update Failed due to " + e);
                e.printStackTrace();
            }
        });
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            this.appUpdateInfo = appUpdateInfo;
            Log.d(TAG, "AppUpdate: Success: updateAvailability:" + appUpdateInfo.updateAvailability()
                    + " IMMEDIATE Type: " + appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                    + " FLEXIBLE Type: " + appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
                    + " VersionCode:" + appUpdateInfo.availableVersionCode()
                    + " CurrentVersionCode:" + BuildConfig.VERSION_CODE
                    + " InstallStatus:" + appUpdateInfo.installStatus());
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED)
                popupSnackbarForCompleteUpdate();
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                Log.d(TAG, "AppUpdate: Update found");
                // Request an immediate update.

                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                    popupSnackbarUpdateAvailable();
                }

            } else {
                Log.d(TAG, "AppUpdate: Update not found");
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void popupSnackbarForCompleteUpdate() {

        try {
            LinearLayout.LayoutParams objLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE);

            // Get the Snackbar layout view
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            layout.setBackgroundResource(R.color.colorUpdateDialog);

            // Set snackbar layout params
            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams) layout.getLayoutParams();
            parentParams.setMargins(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);
            layout.setPadding(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);

            // Inflate our custom view
            View snackView = getLayoutInflater().inflate(R.layout.snack_for_update_complete, null);
            snackView.findViewById(R.id.actionInstall).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    if (appUpdateManager != null) {
                        appUpdateManager.completeUpdate();
                    }

                }
            });

            // Add our custom view to the Snackbar's layout
            layout.addView(snackView, objLayoutParams);
            View view = snackbar.getView();

            // Position snackbar at top
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar.setAnchorView(R.id.bottomNavigationView);
            // Show the Snackbar
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    InstallStateUpdatedListener installStateUpdatedListener = new
            InstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(InstallState state) {
                    if (state.installStatus() == InstallStatus.DOWNLOADED) {
                        popupSnackbarForCompleteUpdate();
                    } else if (state.installStatus() == InstallStatus.INSTALLED) {
                        if (appUpdateManager != null) {
                            appUpdateManager.unregisterListener(installStateUpdatedListener);
                        }

                        // Remove Old Pref and set it into new pref.
                        SharePreferenceManager.copyPref();
                    } else {
                    }
                }
            };

    @SuppressLint("RestrictedApi")
    private void popupSnackbarUpdateAvailable() {

        try {
            LinearLayout.LayoutParams objLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), "", Snackbar.LENGTH_INDEFINITE);

            // Get the Snackbar layout view
            Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
            layout.setBackgroundResource(R.color.colorUpdateDialog);

            // Set snackbar layout params
            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams) layout.getLayoutParams();
            parentParams.setMargins(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);
            layout.setPadding(0, 0, 0, 0);
            layout.setLayoutParams(parentParams);

            // Inflate our custom view
            View snackView = getLayoutInflater().inflate(R.layout.snack_for_update_available, null);
            snackView.findViewById(R.id.actionUpdate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                    if (appUpdateManager != null && appUpdateInfo != null) {
//                        MyApplication.mAppPause = true;
                        try {
                            appUpdateManager.startUpdateFlowForResult(
                                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                    appUpdateInfo,
                                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                    AppUpdateType.FLEXIBLE,
                                    // The current activity making the update request.
                                    MasterActivity.this,
                                    // Include a request code to later monitor this update request.
                                    RC_APP_UPDATE);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            snackView.findViewById(R.id.actionClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
            // Add our custom view to the Snackbar's layout
            layout.addView(snackView, objLayoutParams);

            View view = snackbar.getView();

            // Position snackbar at top
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar.setAnchorView(R.id.bottomNavigationView);


            // Show the Snackbar
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Log.d(TAG, "AppUpdate: onActivityResult: app download failed");
            } else {
                Log.d(TAG, "AppUpdate: onActivityResult: app update failed");
            }
        }
    }
}
