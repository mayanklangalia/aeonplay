package in.aeonplay.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.StartupService;
import in.aeonplay.preferences.SharePreferenceManager;

public class SplashActivity extends MasterActivity {
    protected int splashTime = 4000;
    private Thread splashTread;
    private TextView appTitle;
    private ImageView appLogo;

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(SplashActivity.this, StartupService.class));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setData();
        splashCall();

    }

    private void setData() {
        appLogo = findViewById(R.id.logo);
        appTitle = findViewById(R.id.title);
        appTitle.setTypeface(getLogoFont());

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_splash);
        appLogo.setAnimation(animation);

        Animation animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_splash);
        appTitle.setAnimation(animationFade);
    }

    private void splashCall() {
        final SplashActivity sPlashScreen = this;

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(splashTime);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (!SharePreferenceManager.getString(Constants.TOKEN_ACCESS).equalsIgnoreCase("")) {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, MainActivity.class);
                        startActivity(i);
                        finish();

                    } else {
                        Intent i = new Intent();
                        i.setClass(sPlashScreen, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        };
        splashTread.start();
    }

    private boolean isPermissionGranted() {
        String permission = android.Manifest.permission.READ_PHONE_STATE;
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}