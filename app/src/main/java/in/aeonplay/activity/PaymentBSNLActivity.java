package in.aeonplay.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;

import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Package.PackageChildModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PaymentBSNLActivity extends MasterActivity implements PaymentResultWithDataListener {

    private PackageChildModel packageChildModel;
    private Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Checkout.preload(getApplicationContext());

        bundle = getIntent().getExtras();
        if (bundle != null)
            packageChildModel = bundle.getParcelable(Constants.DATA);

        setData();
    }

    private void setData() {
        startPayment(SharePreferenceManager.getInt(Constants.RPAYMENT_AMOUNT),
                SharePreferenceManager.getString(Constants.RPAYMENT_ORDER_ID));
    }

    private void startPayment(int amount, String orderID) {
        Log.d("onPaymentSP: ", amount + " | " + orderID);
        final Checkout co = new Checkout();
        co.setImage(R.drawable.ic_action_logo);

        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", "Aeon Subscription Package");
            options.put("amount", amount);
            options.put("send_sms_hash",true);
            options.put("payment_capture", true);
            options.put("currency", "INR");
            options.put("order_id", orderID);

            JSONObject notes = new JSONObject();
            notes.put("user_id", String.valueOf(SharePreferenceManager.getUserData().getId()));
            notes.put("package_id", String.valueOf(packageChildModel.getId()));
            Log.d("onPaymentNotes: ", notes.toString());

            options.put("email", SharePreferenceManager.getUserData().getEmail());
            options.put("contact", SharePreferenceManager.getUserData().getMobileNo());
            options.put("notes", notes);

            co.open(PaymentBSNLActivity.this, options);
        } catch (Exception e) {
            Log.d("onPaymentError: ", e.getMessage().toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData paymentData) {
        try {
            Log.d("onPaymentSuccess: ", razorpayPaymentID + " | " + paymentData.getData().toString());
            SharePreferenceManager.save(Constants.RPAYMENT_ORDER_ID, "");
            SharePreferenceManager.save(Constants.RPAYMENT_AMOUNT, 0);
            SharePreferenceManager.save(Constants.RPAYMENT_STATUS, "");
            showAlertDialog("success", razorpayPaymentID);

        } catch (Exception e) {
            Log.d("onPaymentSuccess: ", "Exception: " + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentError(int code, String response, PaymentData paymentData) {
        Log.d("onPaymentError: ", code + " " + response);
        SharePreferenceManager.save(Constants.RPAYMENT_ORDER_ID, "");
        SharePreferenceManager.save(Constants.RPAYMENT_AMOUNT, 0);
        SharePreferenceManager.save(Constants.RPAYMENT_STATUS, "");
        finish();
    }

    private void showAlertDialog(String orderStatus, String paymentID) {
        Dialog dialog = new Dialog(PaymentBSNLActivity.this, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_summary);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
        dialog.setCancelable(false);
        dialog.show();

        TextView txtTransactionAmount = dialog.findViewById(R.id.txtTransactionAmount);
        TextView txtPaymentStatus = dialog.findViewById(R.id.txtPaymentStatus);
        TextView txtTransactionID = dialog.findViewById(R.id.txtTransactionID);
        TextView txtTransactionDate = dialog.findViewById(R.id.txtTransactionDate);
        Button btnOKAY = dialog.findViewById(R.id.btnOKAY);

        String paymentStatus = "";
        if (orderStatus.equalsIgnoreCase("failure")) {
            paymentStatus = "Transaction is Failure";

        } else if (orderStatus.equalsIgnoreCase("success")) {
            paymentStatus = "Transaction is Successful";

        } else {
            paymentStatus = "Transaction is Cancelled";
        }

        txtPaymentStatus.setText(paymentStatus);
        txtTransactionAmount.setText(Html.fromHtml("<sup><small>₹</small></sup><big>" + SharePreferenceManager.getInt(Constants.RPAYMENT_AMOUNT) + "</big>"));
        txtTransactionID.setText(paymentID);
        txtTransactionDate.setText(new SimpleDateFormat("dd MMM, yyyy h:mma", Locale.getDefault()).format(new Date()));

        btnOKAY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmOrder(packageChildModel);
            }
        });
    }

    public void confirmOrder(PackageChildModel packageChildModel) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.confirmOrder(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS),
                String.valueOf(packageChildModel.getId()));

        APIClient.callAPI(PaymentBSNLActivity.this, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Log.d("ConfirmOrder: ", "onSuccess: " + response);

                Intent intent = new Intent(PaymentBSNLActivity.this, MainActivity.class);
                intent.putExtra(Constants.PAYMENT, "PAYMENT");
                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(PaymentBSNLActivity.this).toBundle();
                startActivity(intent, bundle);
                finish();
            }

            @Override
            public void onFailure(String error, int responseCode) {
                Log.d("ConfirmOrder: ", "onFailure: " + error + " " + responseCode);
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                Log.d("ConfirmOrder: ", "onError: " + error);
                showMessageToUser(error);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

