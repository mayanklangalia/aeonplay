package in.aeonplay.activity;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;

import in.aeonplay.BuildConfig;
import in.aeonplay.MyApplication;
import in.aeonplay.R;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Login.CanBundleData;
import in.aeonplay.model.Login.TokenModel;
import in.aeonplay.model.OTPModel.SendReceiveModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.otp.OnChangeListener;
import in.aeonplay.otp.OtpEditText;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class OTPActivity extends MasterActivity {

    private Button btnContinue;
    private Bundle bundle;
    private CanBundleData canBundleData;
    private int comeFrom;
    private OtpEditText otpEditText;
    private Call<ResponseBody> otpCallBack;
    private ProgressBar progressBar;
    private TextView txtResend, txtTimer, txtNotes;
    private int resentCounter = 0;
    private CountDownTimer mCountDownTimer;
    private String username;

    private static final int SMS_CONSENT_REQUEST = 1002;
    private int beginIndex = 0, endIndex = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorTransparent));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorTransparent));
        setContentView(R.layout.activity_otp);

        Init();
        setBundle();
        setSMSReceiver();
    }

    private void setSMSReceiver() {
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsVerificationReceiver, intentFilter);
        SmsRetriever.getClient(this).startSmsUserConsent(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SMS_CONSENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Get SMS message content
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                if (message != null) {
                    otpEditText.setText(parseOneTimeCode(message));
//                    imgOK.performClick();
                }
            }
        }
    }

    private String parseOneTimeCode(String otp) {
        return otp.substring(beginIndex, endIndex);
    }

    private final BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            startActivityForResult(consentIntent, SMS_CONSENT_REQUEST);
                        } catch (ActivityNotFoundException e) {
                            // Handle the exception ...
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };

    private void Init() {
        btnContinue = findViewById(R.id.btnContinue);
        txtResend = findViewById(R.id.txtResend);
        txtTimer = findViewById(R.id.txtTimer);
        txtNotes = findViewById(R.id.txtNotes);
        otpEditText = findViewById(R.id.otpEditText);
        progressBar = findViewById(R.id.progressBar);
    }

    private void setBundle() {
        bundle = getIntent().getExtras();
        if (bundle != null) {
            comeFrom = bundle.getInt(Constants.MODE);
            if (comeFrom == 0) {
                username = bundle.getString(Constants.USERNAME);

            } else {
                canBundleData = bundle.getParcelable(Constants.DATA);
            }

            btnContinue.setEnabled(false);
            btnContinue.setBackgroundResource(R.drawable.button_enable);
            otpEditText.setOnChangeListener(new OnChangeListener() {
                @Override
                public void onChange(String value) {
                    btnContinue.setEnabled(value.length() == otpEditText.getMaxCharLength());
                    btnContinue.setBackgroundResource(R.drawable.button_focus);
//                    if (value.length() == otpEditText.getMaxCharLength()) {
//                        imgOK.performClick();
//                    }
                }
            });

            sendOTP(comeFrom);
            btnContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (comeFrom == 0) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLoginRequest(username, otpEditText.getOtpValue());
                            }
                        }, 1000);

                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getRegisterRequest(canBundleData, otpEditText.getOtpValue());
                            }
                        }, 1000);
                    }
                }
            });

//            otpEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                @Override
//                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                    if (actionId == EditorInfo.IME_ACTION_DONE) {
//                        imgOK.performClick();
//                        return true;
//                    }
//                    return false;
//                }
//            });

            txtResend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resentCounter = resentCounter + 1;
                    if (resentCounter > 3) {
                        showMessageToUser("You have reached max limit!");
                    } else {
                        sendOTP(comeFrom);
                    }
                }
            });
        }
    }

    private void sendOTP(int comeFrom) {
        String isAction = comeFrom == 0 ? "login" : "registration";
        String isMobileNumber = comeFrom == 0 ? username : canBundleData.getMobile();
        txtNotes.setText("Please type the verification code sent to +91 " + isMobileNumber);
        Log.d("TAG", "sendOTP: " + isAction + " > " + isMobileNumber);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserOTP(
                MyApplication.getInstance().getOAuthTokenRequest(),
                isMobileNumber, isAction);

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                SendReceiveModel sendReceiveModel = gson.fromJson(reader, SendReceiveModel.class);

                if (sendReceiveModel.getData() != null) {
                    if (sendReceiveModel.getData().getOtp().getStatus() == true) {
                        showMessageToUser("OTP sent successfully");
                        showCounter();

                    } else {
                        showMessageToUser(sendReceiveModel.getData().getOtp().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                showMessageToUser(error);
            }
        });
    }

    private void getLoginRequest(String username, String userOTP) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserLogin(
                MyApplication.getInstance().getOAuthTokenRequest(),
                BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, getString(R.string.grant_type_otp),
                userOTP, username, username);

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                if (loginModel != null) {
                    SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                    SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                    SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                    SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                    finish();
                    Intent intent = new Intent(OTPActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(OTPActivity.this).toBundle();
                    startActivity(intent, bundle);
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonResponse = new JSONObject(error);
                    JSONObject jsonObject = jsonResponse.optJSONObject("errors");
                    JSONArray jsonArray = jsonObject.optJSONArray("otp");
                    if (jsonArray.get(0).toString().equalsIgnoreCase("otp is missing or expired")) {
                        showMessageToUser("Incorrect OTP. Please try again.");
                    } else {
                        showMessageToUser(jsonArray.get(0).toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                showMessageToUser(error);
            }
        });
    }

    private void getRegisterRequest(CanBundleData canBundleData, String userOTP) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> otpCallBack = apiInterface.postUserRegister(
                MyApplication.getInstance().getOAuthTokenRequest(),
                BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, getString(R.string.grant_type_otp),
                userOTP, canBundleData.getFirstname(), canBundleData.getLastname(), canBundleData.getMobile(),
                canBundleData.getMobile());

        APIClient.callAPI(this, otpCallBack, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                Gson gson = new Gson();
                Reader reader = new StringReader(response);
                TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                if (loginModel != null) {
                    SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                    SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                    SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                    SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                    AlertDialog.Builder builder = new AlertDialog.Builder(OTPActivity.this, R.style.AlertTheme);
                    builder.setMessage("Thanks! Your account has been successfully created. Please login using mobile number.")
                            .setTitle(R.string.app_name);

                    builder.setCancelable(false)
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    finish();
                                    Intent intent = new Intent(OTPActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(OTPActivity.this).toBundle();
                                    startActivity(intent, bundle);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonResponse = new JSONObject(error);
                    JSONObject jsonObject = jsonResponse.optJSONObject("errors");
                    JSONArray jsonArray = jsonObject.optJSONArray("otp");
                    if (jsonArray.get(0).toString().equalsIgnoreCase("otp is missing or expired")) {
                        showMessageToUser("Incorrect OTP. Please try again.");
                    } else {
                        showMessageToUser(jsonArray.get(0).toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                showMessageToUser(error);
            }
        });
    }

    private void showCounter() {
        txtTimer.setVisibility(View.VISIBLE);
        txtResend.setEnabled(false);
        mCountDownTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                txtTimer.setText(formatTime(millisUntilFinished));
            }

            public void onFinish() {
                txtTimer.setText("00:00");
                txtTimer.setVisibility(View.INVISIBLE);
                txtResend.setEnabled(true);
            }
        };
        mCountDownTimer.start();
    }

    void cancelTimer() {
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();
    }

    public String formatTime(long millis) {
        String output;
        long seconds = millis / 1000;
        long minutes = seconds / 60;

        seconds = seconds % 60;
        minutes = minutes % 60;

        String sec = String.valueOf(seconds);
        String min = String.valueOf(minutes);

        if (seconds < 10)
            sec = "0" + seconds;
        if (minutes < 10)
            min = "0" + minutes;

        output = min + " : " + sec;
        return output;
    }

    @Override
    public void onBackPressed() {
        finish();
        cancelTimer();
    }
}