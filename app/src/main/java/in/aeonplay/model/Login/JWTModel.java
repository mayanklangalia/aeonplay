package in.aeonplay.model.Login;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JWTModel implements Parcelable {

    @SerializedName("aud")
    @Expose
    private String aud;
    @SerializedName("jti")
    @Expose
    private String jti;
    @SerializedName("iat")
    @Expose
    private Double iat;
    @SerializedName("nbf")
    @Expose
    private Double nbf;
    @SerializedName("exp")
    @Expose
    private Double exp;
    @SerializedName("sub")
    @Expose
    private String sub;
    @SerializedName("scopes")
    @Expose
    private List<Object> scopes = null;
    public final static Creator<JWTModel> CREATOR = new Creator<JWTModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JWTModel createFromParcel(android.os.Parcel in) {
            return new JWTModel(in);
        }

        public JWTModel[] newArray(int size) {
            return (new JWTModel[size]);
        }

    };

    protected JWTModel(android.os.Parcel in) {
        this.aud = ((String) in.readValue((String.class.getClassLoader())));
        this.jti = ((String) in.readValue((String.class.getClassLoader())));
        this.iat = ((Double) in.readValue((Double.class.getClassLoader())));
        this.nbf = ((Double) in.readValue((Double.class.getClassLoader())));
        this.exp = ((Double) in.readValue((Double.class.getClassLoader())));
        this.sub = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.scopes, (Object.class.getClassLoader()));
    }

    public JWTModel() {
    }

    public String getAud() {
        return aud;
    }

    public void setAud(String aud) {
        this.aud = aud;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public Double getIat() {
        return iat;
    }

    public void setIat(Double iat) {
        this.iat = iat;
    }

    public Double getNbf() {
        return nbf;
    }

    public void setNbf(Double nbf) {
        this.nbf = nbf;
    }

    public Double getExp() {
        return exp;
    }

    public void setExp(Double exp) {
        this.exp = exp;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public List<Object> getScopes() {
        return scopes;
    }

    public void setScopes(List<Object> scopes) {
        this.scopes = scopes;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(aud);
        dest.writeValue(jti);
        dest.writeValue(iat);
        dest.writeValue(nbf);
        dest.writeValue(exp);
        dest.writeValue(sub);
        dest.writeList(scopes);
    }

    public int describeContents() {
        return 0;
    }

}