package in.aeonplay.model.Login;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CanLoginDataModel implements Parcelable {

    @SerializedName("registration")
    @Expose
    private Boolean registration;
    @SerializedName("login")
    @Expose
    private Boolean login;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    public final static Creator<CanLoginDataModel> CREATOR = new Creator<CanLoginDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CanLoginDataModel createFromParcel(android.os.Parcel in) {
            return new CanLoginDataModel(in);
        }

        public CanLoginDataModel[] newArray(int size) {
            return (new CanLoginDataModel[size]);
        }

    };

    protected CanLoginDataModel(android.os.Parcel in) {
        this.registration = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.login = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.mobileNo = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CanLoginDataModel() {
    }

    public Boolean getRegistration() {
        return registration;
    }

    public void setRegistration(Boolean registration) {
        this.registration = registration;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(registration);
        dest.writeValue(mobileNo);
    }

    public int describeContents() {
        return 0;
    }
}