package in.aeonplay.model.Login;

import android.os.Parcel;
import android.os.Parcelable;

public class CanBundleData implements Parcelable {

    private String firstname;
    private String lastname;
    private String email;
    private String mobile;

    public CanBundleData(String fname, String lname, String email, String mobile) {
       this.firstname = fname;
       this.lastname = lname;
       this.email = email;
       this.mobile = mobile;
    }

    public CanBundleData(Parcel in) {
        firstname = in.readString();
        lastname = in.readString();
        email = in.readString();
        mobile = in.readString();
    }

    public static final Creator<CanBundleData> CREATOR = new Creator<CanBundleData>() {
        @Override
        public CanBundleData createFromParcel(Parcel in) {
            return new CanBundleData(in);
        }

        @Override
        public CanBundleData[] newArray(int size) {
            return new CanBundleData[size];
        }
    };

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstname);
        dest.writeString(lastname);
        dest.writeString(email);
        dest.writeString(mobile);
    }
}
