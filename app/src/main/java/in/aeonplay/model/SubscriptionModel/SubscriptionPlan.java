package in.aeonplay.model.SubscriptionModel;


import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionPlan implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("description")
    @Expose
    private List<String> description = null;
    @SerializedName("validity")
    @Expose
    private Integer validity;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("package_group_id")
    @Expose
    private Integer packageGroupId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("content_access")
    @Expose
    private List<ContentAccess> contentAccess = null;
    @SerializedName("packagegroup")
    @Expose
    private Packagegroup packagegroup;
    @SerializedName("mobile_carrier")
    @Expose
    private String mobileCarrier;

    public final static Creator<SubscriptionPlan> CREATOR = new Creator<SubscriptionPlan>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SubscriptionPlan createFromParcel(android.os.Parcel in) {
            return new SubscriptionPlan(in);
        }

        public SubscriptionPlan[] newArray(int size) {
            return (new SubscriptionPlan[size]);
        }

    };

    protected SubscriptionPlan(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.description, (String.class.getClassLoader()));
        this.validity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageGroupId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.contentAccess, (ContentAccess.class.getClassLoader()));
        this.packagegroup = ((Packagegroup) in.readValue((Packagegroup.class.getClassLoader())));
        this.mobileCarrier = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SubscriptionPlan() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPackageGroupId() {
        return packageGroupId;
    }

    public void setPackageGroupId(Integer packageGroupId) {
        this.packageGroupId = packageGroupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ContentAccess> getContentAccess() {
        return contentAccess;
    }

    public void setContentAccess(List<ContentAccess> contentAccess) {
        this.contentAccess = contentAccess;
    }

    public Packagegroup getPackagegroup() {
        return packagegroup;
    }

    public void setPackagegroup(Packagegroup packagegroup) {
        this.packagegroup = packagegroup;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(amount);
        dest.writeList(description);
        dest.writeValue(validity);
        dest.writeValue(status);
        dest.writeValue(packageGroupId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeList(contentAccess);
        dest.writeValue(packagegroup);
        dest.writeValue(mobileCarrier);
    }

    public int describeContents() {
        return 0;
    }

}