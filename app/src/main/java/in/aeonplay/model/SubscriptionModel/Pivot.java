package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot implements Parcelable {

    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("content_provider_id")
    @Expose
    private Integer contentProviderId;
    public final static Creator<Pivot> CREATOR = new Creator<Pivot>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Pivot createFromParcel(android.os.Parcel in) {
            return new Pivot(in);
        }

        public Pivot[] newArray(int size) {
            return (new Pivot[size]);
        }

    };

    protected Pivot(android.os.Parcel in) {
        this.packageId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentProviderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Pivot() {
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getContentProviderId() {
        return contentProviderId;
    }

    public void setContentProviderId(Integer contentProviderId) {
        this.contentProviderId = contentProviderId;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(packageId);
        dest.writeValue(contentProviderId);
    }

    public int describeContents() {
        return 0;
    }

}