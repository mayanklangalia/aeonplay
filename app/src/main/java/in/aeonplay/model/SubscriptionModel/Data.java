package in.aeonplay.model.SubscriptionModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data implements Parcelable {

    @SerializedName("active_subscription")
    @Expose
    private ActiveSubscription activeSubscription;
    @SerializedName("subscription_history")
    @Expose
    private List<ActiveSubscription> subscriptionHistory = null;
    public final static Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(android.os.Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(android.os.Parcel in) {
        this.activeSubscription = ((ActiveSubscription) in.readValue((ActiveSubscription.class.getClassLoader())));
        in.readList(this.subscriptionHistory, (Object.class.getClassLoader()));
    }

    public Data() {
    }

    public ActiveSubscription getActiveSubscription() {
        return activeSubscription;
    }

    public void setActiveSubscription(ActiveSubscription activeSubscription) {
        this.activeSubscription = activeSubscription;
    }

    public List<ActiveSubscription> getSubscriptionHistory() {
        return subscriptionHistory;
    }

    public void setSubscriptionHistory(List<ActiveSubscription> subscriptionHistory) {
        this.subscriptionHistory = subscriptionHistory;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(activeSubscription);
        dest.writeList(subscriptionHistory);
    }

    public int describeContents() {
        return 0;
    }

}