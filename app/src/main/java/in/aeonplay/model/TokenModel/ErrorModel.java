package in.aeonplay.model.TokenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorModel implements Parcelable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("error_description")
    @Expose
    private String errorDescription;
    @SerializedName("message")
    @Expose
    private String message;
    public final static Creator<ErrorModel> CREATOR = new Creator<ErrorModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ErrorModel createFromParcel(Parcel in) {
            return new ErrorModel(in);
        }

        public ErrorModel[] newArray(int size) {
            return (new ErrorModel[size]);
        }

    };

    protected ErrorModel(Parcel in) {
        this.error = ((String) in.readValue((String.class.getClassLoader())));
        this.errorDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ErrorModel() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(errorDescription);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }
}
