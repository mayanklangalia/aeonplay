package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanThumbnail implements Parcelable {

    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("landscape")
    @Expose
    private String landscape;
    @SerializedName("portrait")
    @Expose
    private String portrait;
    public final static Creator<CommanThumbnail> CREATOR = new Creator<CommanThumbnail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommanThumbnail createFromParcel(android.os.Parcel in) {
            return new CommanThumbnail(in);
        }

        public CommanThumbnail[] newArray(int size) {
            return (new CommanThumbnail[size]);
        }

    };

    protected CommanThumbnail(android.os.Parcel in) {
        this.background = ((String) in.readValue((String.class.getClassLoader())));
        this.landscape = ((String) in.readValue((String.class.getClassLoader())));
        this.portrait = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CommanThumbnail() {
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String backgroud) {
        this.background = backgroud;
    }

    public String getLandscape() {
        return landscape;
    }

    public void setLandscape(String landscape) {
        this.landscape = landscape;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(background);
        dest.writeValue(landscape);
        dest.writeValue(portrait);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "CommanThumbnail{" +
                "background='" + background + '\'' +
                ", landscape='" + landscape + '\'' +
                ", portrait='" + portrait + '\'' +
                '}';
    }
}