package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private CommanData data;

    public final static Creator<CommanModel> CREATOR = new Creator<CommanModel>() {

        @SuppressWarnings({
                "unchecked"
        })
        public CommanModel createFromParcel(android.os.Parcel in) {
            return new CommanModel(in);
        }

        public CommanModel[] newArray(int size) {
            return (new CommanModel[size]);
        }

    };

    protected CommanModel(android.os.Parcel in) {
        this.data = ((CommanData) in.readValue((CommanData.class.getClassLoader())));
    }

    public CommanModel() {
    }

    public CommanData getData() {
        return data;
    }

    public void setData(CommanData data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}