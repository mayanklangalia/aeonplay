package in.aeonplay.model.Comman;

import java.util.ArrayList;

public class CommanHeader extends RecyclerViewItem {

    private String title;
    private String nextPage;
    private ArrayList<CommanDataList> commanDataLists = null;
    boolean isAdvertisement = false;
    boolean isHeader = false;
    boolean isNews = false;

    public CommanHeader(String title, String page, ArrayList<CommanDataList> commanDataLists) {
        this.title = title;
        this.nextPage = page;
        this.commanDataLists = commanDataLists;
    }

    public boolean isNews() {
        return isNews;
    }

    public void setNews(boolean news) {
        isNews = news;
    }

    public CommanHeader(boolean advertisement, boolean isHeader, boolean isNews) {
        this.isAdvertisement = advertisement;
        this.isHeader = isHeader;
        this.isNews = isNews;
    }

    public CommanHeader() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public ArrayList<CommanDataList> getDatum() {
        return commanDataLists;
    }

    public void setDatum(ArrayList<CommanDataList> commanDataLists) {
        this.commanDataLists = commanDataLists;
    }

    public boolean isAdvertisement() {
        return isAdvertisement;
    }

    public void setAdvertisement(boolean advertisement) {
        isAdvertisement = advertisement;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }
}