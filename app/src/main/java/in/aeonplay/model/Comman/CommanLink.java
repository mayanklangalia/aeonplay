package in.aeonplay.model.Comman;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommanLink implements Parcelable {

    @SerializedName("url")
    @Expose
    private Object url;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("active")
    @Expose
    private Boolean active;
    public final static Creator<CommanLink> CREATOR = new Creator<CommanLink>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommanLink createFromParcel(android.os.Parcel in) {
            return new CommanLink(in);
        }

        public CommanLink[] newArray(int size) {
            return (new CommanLink[size]);
        }

    };

    protected CommanLink(android.os.Parcel in) {
        this.url = ((Object) in.readValue((Object.class.getClassLoader())));
        this.label = ((String) in.readValue((String.class.getClassLoader())));
        this.active = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public CommanLink() {
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(url);
        dest.writeValue(label);
        dest.writeValue(active);
    }

    public int describeContents() {
        return 0;
    }
}