package in.aeonplay.model.Navigation;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NavigationMenu implements Parcelable {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    public final static Creator<NavigationMenu> CREATOR = new Creator<NavigationMenu>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NavigationMenu createFromParcel(android.os.Parcel in) {
            return new NavigationMenu(in);
        }

        public NavigationMenu[] newArray(int size) {
            return (new NavigationMenu[size]);
        }

    };

    protected NavigationMenu(android.os.Parcel in) {
        in.readList(this.data, (Datum.class.getClassLoader()));
    }

    public NavigationMenu() {
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }
}