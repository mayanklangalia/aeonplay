package in.aeonplay.model.Payment;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RazorpayModel implements Parcelable {
    @Override
    public String toString() {
        return "RazorpayModel{" +
                "id='" + id + '\'' +
                ", entity='" + entity + '\'' +
                ", amount=" + amount +
                ", amountPaid=" + amountPaid +
                ", amountDue=" + amountDue +
                ", currency='" + currency + '\'' +
                ", status='" + status + '\'' +
                ", attempts=" + attempts +
                ", createdAt=" + createdAt +
                '}';
    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("entity")
    @Expose
    private String entity;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("amount_paid")
    @Expose
    private Integer amountPaid;
    @SerializedName("amount_due")
    @Expose
    private Integer amountDue;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("attempts")
    @Expose
    private Integer attempts;
    @SerializedName("created_at")
    @Expose
    private Integer createdAt;
    public final static Creator<RazorpayModel> CREATOR = new Creator<RazorpayModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RazorpayModel createFromParcel(android.os.Parcel in) {
            return new RazorpayModel(in);
        }

        public RazorpayModel[] newArray(int size) {
            return (new RazorpayModel[size]);
        }

    };

    protected RazorpayModel(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.entity = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.amountPaid = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.amountDue = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.currency = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.attempts = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public RazorpayModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Integer amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Integer getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(Integer amountDue) {
        this.amountDue = amountDue;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {
        this.attempts = attempts;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(entity);
        dest.writeValue(amount);
        dest.writeValue(amountPaid);
        dest.writeValue(amountDue);
        dest.writeValue(currency);
        dest.writeValue(status);
        dest.writeValue(attempts);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return 0;
    }

}