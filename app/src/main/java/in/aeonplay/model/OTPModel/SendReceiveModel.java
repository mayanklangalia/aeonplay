package in.aeonplay.model.OTPModel;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendReceiveModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private SendReceiveDataModel sendReceiveDataModel;
    public final static Creator<SendReceiveModel> CREATOR = new Creator<SendReceiveModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SendReceiveModel createFromParcel(android.os.Parcel in) {
            return new SendReceiveModel(in);
        }

        public SendReceiveModel[] newArray(int size) {
            return (new SendReceiveModel[size]);
        }

    };

    protected SendReceiveModel(android.os.Parcel in) {
        this.sendReceiveDataModel = ((SendReceiveDataModel) in.readValue((SendReceiveDataModel.class.getClassLoader())));
    }

    public SendReceiveModel() {
    }

    public SendReceiveDataModel getData() {
        return sendReceiveDataModel;
    }

    public void setData(SendReceiveDataModel sendReceiveDataModel) {
        this.sendReceiveDataModel = sendReceiveDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(sendReceiveDataModel);
    }

    public int describeContents() {
        return 0;
    }

}