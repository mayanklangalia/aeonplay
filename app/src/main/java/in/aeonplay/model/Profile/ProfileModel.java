package in.aeonplay.model.Profile;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel implements Parcelable {

    @SerializedName("data")
    @Expose
    private ProfileDataModel profileDataModel;
    public final static Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProfileModel createFromParcel(android.os.Parcel in) {
            return new ProfileModel(in);
        }

        public ProfileModel[] newArray(int size) {
            return (new ProfileModel[size]);
        }

    };

    protected ProfileModel(android.os.Parcel in) {
        this.profileDataModel = ((ProfileDataModel) in.readValue((ProfileDataModel.class.getClassLoader())));
    }

    public ProfileModel() {
    }

    public ProfileDataModel getData() {
        return profileDataModel;
    }

    public void setData(ProfileDataModel profileDataModel) {
        this.profileDataModel = profileDataModel;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(profileDataModel);
    }

    public int describeContents() {
        return 0;
    }
}