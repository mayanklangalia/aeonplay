package in.aeonplay.model.Package;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackagePivotModel implements Parcelable {

    @SerializedName("package_id")
    @Expose
    private Integer packageId;
    @SerializedName("content_provider_id")
    @Expose
    private Integer contentProviderId;
    @SerializedName("validity")
    @Expose
    private Integer validity;
    public final static Creator<PackagePivotModel> CREATOR = new Creator<PackagePivotModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PackagePivotModel createFromParcel(android.os.Parcel in) {
            return new PackagePivotModel(in);
        }

        public PackagePivotModel[] newArray(int size) {
            return (new PackagePivotModel[size]);
        }

    };

    protected PackagePivotModel(android.os.Parcel in) {
        this.packageId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contentProviderId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.validity = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public PackagePivotModel() {
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getContentProviderId() {
        return contentProviderId;
    }

    public void setContentProviderId(Integer contentProviderId) {
        this.contentProviderId = contentProviderId;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(packageId);
        dest.writeValue(contentProviderId);
        dest.writeValue(validity);
    }

    public int describeContents() {
        return 0;
    }

}