package in.aeonplay.model.Package;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackageDataModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private List<String> description = null;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("packages")
    @Expose
    private List<PackageChildModel> packages = null;
    public final static Creator<PackageDataModel> CREATOR = new Creator<PackageDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PackageDataModel createFromParcel(android.os.Parcel in) {
            return new PackageDataModel(in);
        }

        public PackageDataModel[] newArray(int size) {
            return (new PackageDataModel[size]);
        }

    };

    protected PackageDataModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.description, (String.class.getClassLoader()));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.packages, (PackageChildModel.class.getClassLoader()));
    }

    public PackageDataModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PackageChildModel> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageChildModel> packages) {
        this.packages = packages;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeList(description);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeList(packages);
    }

    public int describeContents() {
        return 0;
    }

}