package in.aeonplay.model.Package;


import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PackageChildModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("description")
    @Expose
    private List<String> description = new ArrayList<>();
    @SerializedName("validity")
    @Expose
    private Integer validity;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("package_group_id")
    @Expose
    private Integer packageGroupId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("mobile_carrier")
    @Expose
    private String mobileCarrier;
    @SerializedName("content_access")
    @Expose
    private List<PackageContentAccess> packageContentAccesses = new ArrayList<>();

    private String headerTitle;

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public final static Creator<PackageChildModel> CREATOR = new Creator<PackageChildModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PackageChildModel createFromParcel(android.os.Parcel in) {
            return new PackageChildModel(in);
        }

        public PackageChildModel[] newArray(int size) {
            return (new PackageChildModel[size]);
        }

    };

    protected PackageChildModel(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.description, (String.class.getClassLoader()));
        this.validity = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.packageGroupId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.mobileCarrier = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.packageContentAccesses, (PackageContentAccess.class.getClassLoader()));
    }

    public PackageChildModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPackageGroupId() {
        return packageGroupId;
    }

    public void setPackageGroupId(Integer packageGroupId) {
        this.packageGroupId = packageGroupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public List<PackageContentAccess> getContentAccess() {
        return packageContentAccesses;
    }

    public void setContentAccess(List<PackageContentAccess> packageContentAccesses) {
        this.packageContentAccesses = packageContentAccesses;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(amount);
        dest.writeList(description);
        dest.writeValue(validity);
        dest.writeValue(status);
        dest.writeValue(packageGroupId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(mobileCarrier);
        dest.writeList(packageContentAccesses);
    }

    public int describeContents() {
        return 0;
    }

}