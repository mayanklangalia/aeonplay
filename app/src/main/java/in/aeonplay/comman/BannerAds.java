package in.aeonplay.comman;

import android.content.Context;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import in.aeonplay.R;

public class BannerAds {

    public static void ShowBannerAds(Context context, FrameLayout mAdViewLayout) {

        AdView mAdView = new AdView(context);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(context.getString(R.string.banner_home_footer));
        AdRequest.Builder builder = new AdRequest.Builder();
        mAdView.loadAd(builder.build());
        mAdViewLayout.addView(mAdView);
    }
}
