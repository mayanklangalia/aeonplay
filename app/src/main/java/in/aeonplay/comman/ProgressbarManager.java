package in.aeonplay.comman;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import in.aeonplay.R;

public class ProgressbarManager extends Dialog {

    public ProgressbarManager(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setBackgroundColor(Color.TRANSPARENT);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        TextView textView = new TextView(context);
        textView.setTextColor(Color.WHITE);
        textView.setText("Please wait..");

        ImageView imageView = new ImageView(context);
        Glide.with(context)
                .load(R.raw.loading4)
                .into(imageView);

        layout.addView(imageView, params);
        layout.addView(textView, params);
        addContentView(layout, params);
    }
}