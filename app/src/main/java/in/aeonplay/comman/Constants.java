package in.aeonplay.comman;

public interface Constants {

    int INTERVAL = 2000;
    String NAME = "NAME";
    String EMAIL = "EMAIL";
    String MODE = "MODE";
    String USERNAME = "USERNAME";
    String LOGIN = "LOGIN";
    String TOKEN_TYPE = "TOKEN_TYPE";
    String TOKEN_ACCESS = "TOKEN_ACCESS";
    String TOKEN_REFRESH = "TOKEN_REFRESH";
    String TOKEN_EXPIRES_IN = "TOKEN_EXPIRES_IN";
    String DATA = "DATA";
    String TRAILER = "TRAILER";
    String TITLE = "TITLE";
    String SHOW_ID = "SHOW_ID";
    String PROVIDER = "PROVIDER";
    String PAGE = "PAGE";
    String CATEGORY_ID = "CATEGORY_ID";
    String PAYMENT = "PAYMENT";
    String RPAYMENT_ORDER_ID = "RPAYMENT_ORDER_ID";
    String RPAYMENT_AMOUNT = "RPAYMENT_AMOUNT";
    String RPAYMENT_STATUS = "RPAYMENT_STATUS";
    String PDF_ROOT_URL = "https://docs.google.com/gview?embedded=true&url=";
    String EROS_PARTNER_EPISODE = "https://staging.mzaalo.com/partner-api/getDataSeries?asset_id=";
    String EROS_PARTNER_PLAYBACK = "https://staging.mzaalo.com/partner-api/player/details?";

}
