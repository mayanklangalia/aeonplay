package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;

public class AdvanceSearchAdapter extends RecyclerView.Adapter<AdvanceSearchAdapter.SingleCheckViewHolder> {

    private ArrayList<String> mSingleCheckList;
    private MasterActivity mContext;
    private int checkedPosition = -1;
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }


    public AdvanceSearchAdapter(MasterActivity context, ArrayList<String> listItems) {
        mContext = context;
        mSingleCheckList = listItems;
    }

    @Override
    public SingleCheckViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.item_adv_search, viewGroup, false);
        return new SingleCheckViewHolder(view);
    }

    public void setSelectedPosition(int selectedPosition) {
        checkedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(SingleCheckViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        String item = mSingleCheckList.get(position);
        viewHolder.mMenuTitle.setText(item);

        if (checkedPosition == -1) {
            viewHolder.mMenuTitle.setSelected(false);
            viewHolder.itemView.setBackgroundResource(R.drawable.tag_normal);

        } else {
            if (checkedPosition == position) {
                viewHolder.mMenuTitle.setSelected(true);
                viewHolder.itemView.setBackgroundResource(R.drawable.tag_focus);

            } else {
                viewHolder.mMenuTitle.setSelected(false);
                viewHolder.itemView.setBackgroundResource(R.drawable.tag_normal);
            }
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.mMenuTitle.setSelected(true);
                viewHolder.itemView.setBackgroundResource(R.drawable.tag_focus);

                if (checkedPosition != position) {
                    notifyItemChanged(checkedPosition);
                    checkedPosition = position;
                }

                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(view, checkedPosition);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSingleCheckList.size();
    }

    class SingleCheckViewHolder extends RecyclerView.ViewHolder {
        private TextView mMenuTitle;

        public SingleCheckViewHolder(View itemView) {
            super(itemView);
            mMenuTitle = itemView.findViewById(R.id.tag_name);
        }
    }
}
