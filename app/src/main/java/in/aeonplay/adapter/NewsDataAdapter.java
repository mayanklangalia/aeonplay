package in.aeonplay.adapter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.VerticalTextView;
import in.aeonplay.fragment.PDFFragment;
import in.aeonplay.model.NewsModel.NewsData;

public class NewsDataAdapter extends RecyclerView.Adapter<NewsDataAdapter.ItemRowHolder> {

    private MainActivity context;
    private List<NewsData> mItems;

    public NewsDataAdapter(MainActivity context, List<NewsData> item) {
        this.context = context;
        this.mItems = item;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, final int selectedItem) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_news_item, null);
        ItemRowHolder viewHolder = new ItemRowHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder itemRowHolder, @SuppressLint("RecyclerView") final int position) {
        NewsData newsData = mItems.get(position);

        itemRowHolder.itemTime.setText(new SimpleDateFormat("dd-MMM, yy", Locale.getDefault()).format(new Date()));
        itemRowHolder.itemImageLand.setText(context.titleCase(newsData.getTitle()));
        itemRowHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", newsData);

                PDFFragment pdfFragment = new PDFFragment();
                pdfFragment.setArguments(bundle);
                context.addFragment(pdfFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemRowHolder extends RecyclerView.ViewHolder {

        private TextView itemImageLand;
        private VerticalTextView itemTime;

        public ItemRowHolder(View view) {
            super(view);
            this.itemImageLand = view.findViewById(R.id.itemImageLand);
            this.itemTime = view.findViewById(R.id.itemTime);
        }
    }
}