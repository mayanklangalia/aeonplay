package in.aeonplay.adapter;

import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.NativeAds;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.fragment.MoreFragment;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanHeader;
import in.aeonplay.model.Comman.CommanModel;
import in.aeonplay.model.NewsModel.NewsModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_AD = 0;
    private static final int ITEM_VIEW = 1;
    private static final int ITEM_LOADING = 2;
    private static final int ITEM_HEADER = 3;
    private static final int ITEM_NEWS = 4;
    private final MainActivity mContext;
    private final List<CommanHeader> commanHeaders;
    private CountDownTimer countDownTimer;
    private int advertisementIndex;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    public HomeAdapter(MainActivity activity) {
        this.mContext = activity;
        this.commanHeaders = new ArrayList<>();
    }

    public List<CommanHeader> getMovies() {
        return commanHeaders;
    }

    public void add(CommanHeader r) {
        commanHeaders.add(r);
        //notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<CommanHeader> moveResults) {
        for (CommanHeader result : moveResults) {
            add(result);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CommanHeader());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = commanHeaders.size() - 1;
        CommanHeader result = getItem(position);

        if (result != null) {
            commanHeaders.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CommanHeader getItem(int position) {
        return commanHeaders.get(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM_AD:
                View viewAd = inflater.inflate(R.layout.card_adview_item, viewGroup, false);
                viewHolder = new AdRowHolder(viewAd);
                break;
            case ITEM_VIEW:
                View viewModel = inflater.inflate(R.layout.card_home_item, viewGroup, false);
                viewHolder = new ItemRowHolder(viewModel);
                break;
            case ITEM_LOADING:
                View viewLoading = inflater.inflate(R.layout.card_progress_item, viewGroup, false);
                viewHolder = new LoadingHolder(viewLoading);
                break;
            case ITEM_HEADER:
                View viewHeader = inflater.inflate(R.layout.card_header, viewGroup, false);
                viewHolder = new HeaderRowHolder(viewHeader);
                break;
            case ITEM_NEWS:
                View viewHeaderNews = inflater.inflate(R.layout.card_news_section, viewGroup, false);
                viewHolder = new NewsRowHolder(viewHeaderNews);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ITEM_AD:
                AdRowHolder adRowHolder = (AdRowHolder) holder;
                NativeAds.ShowNativeAdsIndex(mContext, adRowHolder.adsContainer, position);
                break;

            case ITEM_VIEW:
                CommanHeader commanHeader = getMovies().get(position);
                ItemRowHolder itemRowHolder = (ItemRowHolder) holder;

                itemRowHolder.itemTitle.setText(Html.fromHtml("<b><font color=#ffffff>" + commanHeader.getTitle().split("_")[0] + "</font></b>"));
                HomeChildAdapter itemListDataAdapter = new HomeChildAdapter(mContext, commanHeader.getDatum());

                itemRowHolder.mRecyclerChildItem.setLayoutManager(
                        new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                itemRowHolder.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
                itemRowHolder.mRecyclerChildItem.setAdapter(itemListDataAdapter);

                itemRowHolder.itemMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.TITLE, commanHeader.getTitle().split("_")[0]);
                        bundle.putParcelableArrayList(Constants.DATA, commanHeader.getDatum());
                        bundle.putString(Constants.PAGE, commanHeader.getNextPage());
                        bundle.putInt(Constants.CATEGORY_ID, Integer.parseInt(commanHeader.getTitle().split("_")[1]));

                        MoreFragment moreFragment = new MoreFragment();
                        moreFragment.setArguments(bundle);
                        mContext.addFragment(moreFragment);
                    }
                });
                break;

            case ITEM_LOADING:
                LoadingHolder loadingVH = (LoadingHolder) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

//                    loadingVH.mErrorTxt.setText(
//                            errorMsg != null ?
//                                    errorMsg :
//                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;

            case ITEM_HEADER:
                HeaderRowHolder headerRowHolder = (HeaderRowHolder) holder;
                getDashboardVPList(headerRowHolder);
                break;

            case ITEM_NEWS:
                NewsRowHolder newsRowHolder = (NewsRowHolder) holder;
                getNewsList(newsRowHolder);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + getItemViewType(position));
        }
    }

    private void getNewsList(NewsRowHolder newsRowHolder) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getNews(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
//                rootContainer.setVisibility(View.VISIBLE);
//                progressbarManager.dismiss();

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final NewsModel newsModel = gson.fromJson(reader, NewsModel.class);

                    newsRowHolder.itemTitle.setText(Html.fromHtml("<b><font color=#ffffff>" + "Read Newspapers" + "</font></b>"));
                    NewsDataAdapter newsDataAdapter = new NewsDataAdapter(mContext, newsModel.getData());

                    newsRowHolder.mRecyclerChildItem.setLayoutManager(
                            new GridLayoutManager(mContext, 1, GridLayoutManager.HORIZONTAL, false));
                    newsRowHolder.mRecyclerChildItem.setItemAnimator(new DefaultItemAnimator());
                    newsRowHolder.mRecyclerChildItem.setAdapter(newsDataAdapter);
                    newsDataAdapter.notifyDataSetChanged();

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                Log.d("TAG_NEWS: ", "onFailure: " + error + " " + responseCode);
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                Log.d("TAG_NEWS: ", "onError: " + error);
                mContext.showMessageToUser(error);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        CommanHeader recyclerViewItem = getMovies().get(position);

        if (recyclerViewItem instanceof CommanHeader) {
            if (((CommanHeader) recyclerViewItem).isAdvertisement())
                return ITEM_AD;
            if (((CommanHeader) recyclerViewItem).isHeader())
                return ITEM_HEADER;
            if (((CommanHeader) recyclerViewItem).isNews())
                return ITEM_NEWS;
            else
                return (position == getMovies().size() - 1 && isLoadingAdded) ? ITEM_LOADING : ITEM_VIEW;

        } else
            return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return getMovies() == null ? 0 : getMovies().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected ImageView itemMore;
        protected RecyclerView mRecyclerChildItem;
        protected LinearLayout linearSectionItem;

        public ItemRowHolder(View view) {
            super(view);
            this.linearSectionItem = view.findViewById(R.id.linearSectionItem);
            this.itemTitle = view.findViewById(R.id.itemTitle);
            this.itemMore = view.findViewById(R.id.itemMore);
            this.mRecyclerChildItem = view.findViewById(R.id.recycler_childItem);
            mRecyclerChildItem.addItemDecoration(new SpaceItemDecoration());
        }
    }

    public static class AdRowHolder extends RecyclerView.ViewHolder {

        protected FrameLayout adsContainer;

        public AdRowHolder(View view) {
            super(view);
            this.adsContainer = view.findViewById(R.id.adsContainer);
        }
    }

    public static class HeaderRowHolder extends RecyclerView.ViewHolder {

        private ViewPager mViewPager;
        private TextView mAdvertisementTitle, mAdvertisementGenre;
        private Button btnWatchNow;
        private LinearLayout linearMyList, linearInfo;

        public HeaderRowHolder(View view) {
            super(view);
            mViewPager = view.findViewById(R.id.viewPager);
            linearMyList = view.findViewById(R.id.linearMyList);
            linearInfo = view.findViewById(R.id.linearInfo);
            mAdvertisementTitle = view.findViewById(R.id.txtAdvertisementTitle);
            mAdvertisementGenre = view.findViewById(R.id.txtAdvertisementGenre);
            btnWatchNow = view.findViewById(R.id.btnWatchNow);
        }
    }

    protected class LoadingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadMORE_progress);
            mRetryBtn = itemView.findViewById(R.id.loadMORE_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMORE_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadMORE_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadMORE_retry:
                case R.id.loadMORE_errorlayout:

                    showRetry(false, null, getAdapterPosition());
//                    mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public static class NewsRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;
        protected RecyclerView mRecyclerChildItem;
        protected LinearLayout linearSectionItem;

        public NewsRowHolder(View view) {
            super(view);
            this.linearSectionItem = view.findViewById(R.id.linearSectionItem);
            this.itemTitle = view.findViewById(R.id.itemTitle);
            this.mRecyclerChildItem = view.findViewById(R.id.recycler_childItem);
            mRecyclerChildItem.addItemDecoration(new SpaceItemDecoration());
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg, int position) {
        retryPageLoad = show;
        notifyItemChanged(commanHeaders.get(position).getDatum().size() - 1);

//        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    private void getDashboardVPList(HeaderRowHolder headerRowHolder) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getAeonComingSoonList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(mContext, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final CommanModel commanModel = gson.fromJson(reader, CommanModel.class);

                    if (commanModel.getData() != null) {
                        Collections.shuffle(commanModel.getData().getData());
                        PagerAdapter adapter = new AdvertisementAdapter(mContext, commanModel.getData().getData());
                        headerRowHolder.mViewPager.setAdapter(adapter);
                        headerRowHolder.mViewPager.setOffscreenPageLimit(3);
                        headerRowHolder.mViewPager.setPageMargin((int) mContext.getResources().getDimension(com.intuit.sdp.R.dimen._7sdp));
                        headerRowHolder.mViewPager.setClipChildren(false);
                        autoScroll(headerRowHolder, commanModel.getData().getData());

                        advertisementInfo(headerRowHolder, commanModel.getData().getData(), 0);
                        headerRowHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                advertisementInfo(headerRowHolder, commanModel.getData().getData(), position);
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    mContext.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                mContext.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mContext.showMessageToUser(error);
            }
        });
    }

    private void autoScroll(HeaderRowHolder headerRowHolder, List<CommanDataList> promotions) {
        if (promotions.size() > 0){
            countDownTimer = new CountDownTimer(20000, 1000) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    // For Random Position ...
                    Random rand = new Random();
                    advertisementIndex = rand.nextInt(promotions.size());

                    // For Normal Position ...
                    // if (advertisementIndex >= (promotions.size() - 1)) {
                    //     advertisementIndex = 0;
                    // } else {
                    //     advertisementIndex = advertisementIndex + 1;
                    //}

                    headerRowHolder.mViewPager.setCurrentItem(advertisementIndex, true);
                    countDownTimer.start();
                }
            };

            countDownTimer.start();
        }
    }

    private void advertisementInfo(HeaderRowHolder headerRowHolder, List<CommanDataList> resultModelList, int position) {
//        StringBuilder stringBuilder = new StringBuilder();
//        for (String genre : resultModelList.get(position).getGenre()) {
//            stringBuilder.append(genre).append(", ");
//        }
//
//        if (stringBuilder.length() > 0) {
//            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
//        }
//
        headerRowHolder.mAdvertisementGenre.setText(resultModelList.get(position).getDiscription());
        headerRowHolder.mAdvertisementTitle.setText(resultModelList.get(position).getTitle());

        headerRowHolder.linearMyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Coming soon", Toast.LENGTH_SHORT).show();
            }
        });

        headerRowHolder.linearInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("DATA", resultModelList.get(position));

                DetailFragment detailFragment = new DetailFragment();
                detailFragment.setArguments(bundle);
                mContext.addFragment(detailFragment);
            }
        });

        headerRowHolder.btnWatchNow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                CommanDataList commanDataList = resultModelList.get(position);
                mContext.requestDeepLink(commanDataList);
            }
        });
    }

}