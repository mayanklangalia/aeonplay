package in.aeonplay.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

import java.util.List;

import in.aeonplay.R;
import in.aeonplay.model.Comman.CommanDataList;

public class AdvertisementAdapter extends PagerAdapter {

    private List<CommanDataList> mItems;
    private Activity mContext;

    public AdvertisementAdapter(Activity context, List<CommanDataList> items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        CommanDataList commanDataList = mItems.get(position);

        CardView layout = (CardView) mContext.getLayoutInflater()
                .inflate(R.layout.card_advertisement_item, container, false);
        container.addView(layout);

        ImageView imageView = layout.findViewById(R.id.advertisementImage);
        TextView viewPagerCounter = layout.findViewById(R.id.viewPagerCounter);

        viewPagerCounter.setText((position + 1) + "/" + getCount());

        Glide.with(mContext)
                .load(commanDataList.getThumbnail().getPortrait())
                .placeholder(R.drawable.ic_action_noimage)
                .into(imageView);

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
