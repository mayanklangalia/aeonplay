package in.aeonplay.adapter;

public interface PaginationAdapterCallback {
    void retryPageLoad();
}
