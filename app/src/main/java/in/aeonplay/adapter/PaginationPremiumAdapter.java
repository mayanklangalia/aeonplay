package in.aeonplay.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.fragment.DetailFragment;
import in.aeonplay.model.Comman.CommanDataList;

public class PaginationPremiumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<CommanDataList> movieResults;
    private MainActivity mContext;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    public PaginationPremiumAdapter(MainActivity context) {
        this.mContext = context;
        movieResults = new ArrayList<>();
    }

    public List<CommanDataList> getMovies() {
        return movieResults;
    }

    public void setMovies(List<CommanDataList> movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.card_premium_item, parent, false);
                viewHolder = new MovieVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.card_progress_item, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CommanDataList commanDataModel = getMovies().get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final MovieVH movieVH = (MovieVH) holder;

//                movieVH.appImage.getLayoutParams().width = mContext.getResources().getDimensionPixelSize(com.intuit.sdp.R.dimen._90sdp);
//                movieVH.appImage.getLayoutParams().height = mContext.getResources().getDimensionPixelSize(com.intuit.sdp.R.dimen._127sdp);

//                if (position == 0) {
//                    DisplayMetrics displaymetrics = new DisplayMetrics();
//                    mContext.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//                    int height = (int) ((int) displaymetrics.heightPixels * 0.60);
//                    int width = displaymetrics.widthPixels;
//
//                    movieVH.appImage.getLayoutParams().width = width;
//                    movieVH.appImage.getLayoutParams().height = height;
//                }

                Glide.with(mContext)
                        .load(commanDataModel.getThumbnail().getPortrait())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ic_action_noimage)
                        .into(movieVH.appImage);

                movieVH.itemView.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View v) {

                        if (commanDataModel.getProvider().equalsIgnoreCase("youtube")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + commanDataModel.getHostUrl().split("=")[1]));
                            intent.putExtra("force_fullscreen", true);
                            intent.putExtra("finish_on_ended", true);
                            mContext.startActivity(intent);

                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("DATA", commanDataModel);

                            DetailFragment detailFragment = new DetailFragment();
                            detailFragment.setArguments(bundle);
                            mContext.addFragment(detailFragment);
                        }
                    }
                });


                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

//                    loadingVH.mErrorTxt.setText(
//                            errorMsg != null ?
//                                    errorMsg :
//                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(CommanDataList r) {
        movieResults.add(r);
        //notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<CommanDataList> moveResults) {
        for (CommanDataList result : moveResults) {
            add(result);
        }
    }

    public void remove(CommanDataList r) {
        int position = movieResults.indexOf(r);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CommanDataList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        CommanDataList result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public CommanDataList getItem(int position) {
        return movieResults.get(position);
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(movieResults.size() - 1);

//        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    protected class MovieVH extends RecyclerView.ViewHolder {
        protected ImageView appImage;

        public MovieVH(View view) {
            super(view);

            this.appImage = view.findViewById(R.id.itemImage);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadMORE_progress);
            mRetryBtn = itemView.findViewById(R.id.loadMORE_retry);
            mErrorTxt = itemView.findViewById(R.id.loadMORE_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadMORE_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadMORE_retry:
                case R.id.loadMORE_errorlayout:

                    showRetry(false, null);
//                    mCallback.retryPageLoad();
                    break;
            }
        }
    }

}
