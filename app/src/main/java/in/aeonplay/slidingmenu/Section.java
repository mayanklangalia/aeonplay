package in.aeonplay.slidingmenu;

public class Section {

    private final String name;
    private final int image;
    public boolean isExpanded;

    public Section(String name, int image, boolean expand) {
        this.name = name;
        this.image = image;
        isExpanded = expand;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public boolean isExpanded() {
        return isExpanded;
    }
}