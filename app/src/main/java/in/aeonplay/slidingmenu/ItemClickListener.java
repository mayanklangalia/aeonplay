package in.aeonplay.slidingmenu;

public interface ItemClickListener {
    void itemClicked(Item item);
    void itemClicked(Section section);
}
