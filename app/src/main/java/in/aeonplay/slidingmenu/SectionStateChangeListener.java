package in.aeonplay.slidingmenu;

public interface SectionStateChangeListener {
    void onSectionStateChanged(Section section, boolean isOpen);
}