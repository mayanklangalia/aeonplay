package in.aeonplay.payment;

public enum AppEnvironment {

    SANDBOX {
        @Override
        public String merchant_id() {
            return "5436060";
        }

        @Override
        public String merchant_Key() {
            return "oZ7oo9";
        }

        @Override
        public String furl() {
            return "https://development.aeongroup.in/payment/failed";
        }

        @Override
        public String surl() {
            return "https://development.aeongroup.in/payment/success";
        }

        @Override
        public String salt() {
            return "UkojH5TS";
        }

        @Override
        public boolean debug() {
            return true;
        }
    },

    PRODUCTION {
        @Override
        public String merchant_id() {
            return "5436060";
        }

        @Override
        public String merchant_Key() {
            return "cuvDmJKt";
        }

        @Override
        public String furl() {
            return "https://production.aeongroup.in/payment/failed";
        }

        @Override
        public String surl() {
            return "https://production.aeongroup.in/payment/success";
        }

        @Override
        public String salt() {
            return "oxa8Tb4Rmz";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_id();

    public abstract String merchant_Key();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();

}