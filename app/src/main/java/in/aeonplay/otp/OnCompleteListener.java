package in.aeonplay.otp;

public interface OnCompleteListener {
    void onComplete(String value);
}