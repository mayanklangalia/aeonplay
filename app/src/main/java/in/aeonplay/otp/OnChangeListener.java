package in.aeonplay.otp;

public interface OnChangeListener {
    void onChange(String value);
}