package in.aeonplay.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.NativeAds;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends BaseFragment {

    private Bundle bundle;
    private String TAG = DetailFragment.class.getSimpleName();
    private ImageView moviePoster, providerImage, imgExpand, imgPlay;
    private TextView content_name, content_other,
            content_cast, content_director, content_language, txtPlay;
    private TextView content_info;
    private LinearLayout linearEpisode, linearShare;
    private boolean isExpanded = true;
    private FrameLayout adContainer;
    private CommanDataList commanDataList;
    private String youtubeURL;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        bundle = getArguments();
        if (bundle != null) {
            commanDataList = bundle.getParcelable("DATA");
        }

        Init(v);
        setData();
    }

    private void setData() {
        if (commanDataList.getProvider().equalsIgnoreCase("youtube"))
            getTrailer(commanDataList.getHostUrl());

        if (commanDataList.getProvider().equalsIgnoreCase("erosnow")) {
            imgPlay.setVisibility(View.VISIBLE);
            txtPlay.setVisibility(View.VISIBLE);

            if (commanDataList.getContentType().equalsIgnoreCase("original")) {
                imgPlay.setVisibility(View.GONE);
                txtPlay.setVisibility(View.GONE);
            }

        } else {
            imgPlay.setVisibility(View.VISIBLE);
            txtPlay.setVisibility(View.VISIBLE);
        }

        if (commanDataList.getThumbnail().getLandscape() != null) {
            Glide.with(getActivity())
                    .load(commanDataList.getThumbnail().getLandscape())
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .placeholder(R.drawable.ic_action_noimage_land)
                    .into(moviePoster);

        } else {
            Glide.with(getActivity())
                    .load(commanDataList.getThumbnail().getBackground())
                    .transition(GenericTransitionOptions.with(R.anim.fade_in))
                    .placeholder(R.drawable.ic_action_noimage_land)
                    .into(moviePoster);
        }

        providerImage.setVisibility(View.GONE);
        if (commanDataList.getProvider().equalsIgnoreCase("sonyliv")) {
            providerImage.setImageResource(R.drawable.ic_action_crown);
            providerImage.setVisibility(View.VISIBLE);
        }

        stringDecode(content_info, commanDataList.getDiscription());
        if (commanDataList.getLongDescription() != null)
            stringDecode(content_info, commanDataList.getLongDescription());

        stringDecode(content_name, commanDataList.getTitle());
        stringDecode(content_language, "Language: " + commanDataList.getLanguage());

        if (commanDataList.getCastAndCrew() != null) {
            if (commanDataList.getCastAndCrew().contains("|")) {
                String castCrew[] = commanDataList.getCastAndCrew().split("\\|");
                stringDecode(content_cast, castCrew[0].trim().replaceAll("|", ""));
                stringDecode(content_director, castCrew[1].trim());
                content_director.setVisibility(View.VISIBLE);

            } else {
                content_director.setVisibility(View.GONE);
                stringDecode(content_cast, commanDataList.getCastAndCrew());
            }

        } else {
            content_cast.setVisibility(View.GONE);
            content_director.setVisibility(View.GONE);
        }

        linearEpisode.setVisibility(View.GONE);
        if (commanDataList.getContentType().equalsIgnoreCase("show") ||
                commanDataList.getContentType().equalsIgnoreCase("episode") ||
                commanDataList.getContentType().equalsIgnoreCase("original"))
            linearEpisode.setVisibility(View.VISIBLE);

        StringBuilder stringBuilder = new StringBuilder();
        if (commanDataList.getGenre()!=null) {
            for (String genre : commanDataList.getGenre()) {
                stringBuilder.append(genre).append(", ");
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            }
        }

//        String time = "";
//        if (!commanDataList.getDuration().equalsIgnoreCase("0") &&
//                commanDataList.getDuration() != null) {
//            int timeline = Integer.parseInt(commanDataList.getDuration());
//            int hours = timeline / 3600;
//            int minutes = (timeline % 3600) / 60;
//            time = " | " + hours + "h " + minutes + "m";
//        }

        String releaseYear = "";
        if (commanDataList.getReleaseYear() != null) {
            releaseYear = " | " + commanDataList.getReleaseYear();
        }

        content_other.setText(stringBuilder.toString() + releaseYear);

        configureExpandView();
        imgPlay.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                activity.requestDeepLink(commanDataList);
            }
        });

        linearShare.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                shareLink(commanDataList.getTitle(), String.valueOf(commanDataList.getContentId()));
            }
        });

        linearEpisode.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.SHOW_ID, commanDataList.getShowId());
                bundle.putString(Constants.PROVIDER, commanDataList.getProvider());
                EpisodeFragment episodeFragment = new EpisodeFragment();
                episodeFragment.setArguments(bundle);
                activity.addFragment(episodeFragment);
            }
        });
    }

    private void shareLink(String title, String id) {
        try {
            Uri bitmapUri = saveImageExternal(getBitmapFromView(moviePoster));
            Intent shareIntent = ShareCompat.IntentBuilder.from(activity)
                    .setType("*/*")
                    .setStream(bitmapUri)
                    .setText("Check out " + title + " on Aeon Play! " + BuildConfig.AUTHORIZE_URL + id)
                    .getIntent();

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(shareIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri saveImageExternal(Bitmap image) throws Exception {
        File file = new File(getActivity().getCacheDir(), "aeonplay.jpeg");
        FileOutputStream stream = new FileOutputStream(file);
        image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        stream.close();
        Uri uri = FileProvider.getUriForFile(
                getActivity(),
                getActivity().getPackageName() + ".provider", //(use your app signature + ".provider" )
                file);
        ;

        return uri;
    }

    public Bitmap getBitmapFromView(View view) {
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            Bitmap bitmap = imageView.getDrawingCache();
            if (bitmap == null) {
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                bitmap = drawable.getBitmap();
            }
            if (bitmap != null)
                return bitmap;
        }
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap((int) getResources().getDimension(com.intuit.sdp.R.dimen._500sdp), (int) getResources().getDimension(com.intuit.sdp.R.dimen._300sdp), Bitmap.Config.ARGB_8888);
        //Bind a canvas to itview.getLayoutParams().width
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    private void configureExpandView() {
        content_info.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                content_info.getViewTreeObserver().removeOnPreDrawListener(this);

                imgExpand.setVisibility(View.GONE);
                if (content_info.getLineCount() > 5) {
                    content_info.setMaxLines(5);
                    imgExpand.setVisibility(View.VISIBLE);
                }

                return true;
            }
        });

        imgExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpanded) {
                    imgExpand.setImageResource(R.drawable.ic_action_up);
                    content_info.setMaxLines(Integer.MAX_VALUE);
                    content_info.setEllipsize(null);

                } else {
                    imgExpand.setImageResource(R.drawable.ic_action_down);
                    content_info.setMaxLines(5);
                    content_info.setEllipsize(TextUtils.TruncateAt.END);
                }

                isExpanded = !isExpanded;
            }
        });
    }

    private void Init(View v) {
        adContainer = v.findViewById(R.id.adContainer);
        NativeAds.ShowTextNativeAds((MasterActivity) getActivity(), adContainer);

        moviePoster = v.findViewById(R.id.moviePoster);
        providerImage = v.findViewById(R.id.providerImage);

        content_name = v.findViewById(R.id.content_name);
        content_other = v.findViewById(R.id.content_other);
        content_info = v.findViewById(R.id.content_info);
        content_cast = v.findViewById(R.id.content_cast);
        content_director = v.findViewById(R.id.content_director);
        content_language = v.findViewById(R.id.content_language);
        txtPlay = v.findViewById(R.id.txtPlay);
        imgExpand = v.findViewById(R.id.imgExpand);
        imgPlay = v.findViewById(R.id.imgPlay);
        linearEpisode = v.findViewById(R.id.linearEpisode);
        linearShare = v.findViewById(R.id.linearShare);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) content_name.getLayoutParams();
        layoutParams.topMargin = activity.getStatusBarHeight();
        content_name.setLayoutParams(layoutParams);
    }

    public void stringDecode(TextView textView, String s) {
        try {
            textView.setText(s);
//            textView.setText(URLDecoder.decode(s.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    private void getTrailer(String movID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.getTrailer(movID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {

                        try {
                            String res = response.body().string();

                            JSONObject jsonObject = new JSONObject(res);
                            JSONArray jsonArray = jsonObject.getJSONArray("results");

                            if (jsonArray.length() > 0) {
                                JSONObject jObj = jsonArray.getJSONObject(0);
                                youtubeURL = jObj.optString("key");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    Log.d("Exceptions: ", exception.getMessage().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Exceptions: ", t.getMessage().toString());
            }
        });
    }
}

