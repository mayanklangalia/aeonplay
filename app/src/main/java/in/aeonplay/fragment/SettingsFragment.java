package in.aeonplay.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.LoginActivity;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Profile.ProfileDataModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class SettingsFragment extends BaseFragment implements View.OnClickListener {

    private CardView mProfileCard;
    private TextView txtProfileName, txtProfileMobile, txtVersion;
    private CardView cardMyPlan, cardTerms, cardPrivacy, cardRefund, cardAbout, cardContact, cardLogut;
    private ProgressBar logoutProgress;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(false);

        Init(v);
        setData();
    }

    private void setData() {
        ProfileDataModel profileDataModel = SharePreferenceManager.getUserData();
        String text = "<big><font color=" + getResources().getColor(R.color.colorPureWhite) + "> " + profileDataModel.getFirstName() + "</font></big><br>" +
                "<small><font color=" + getResources().getColor(R.color.colorGray) + "> " + profileDataModel.getLastName() + "</font></small>";
        txtProfileName.setText(Html.fromHtml(text));
        txtProfileMobile.setText(profileDataModel.getMobileNo());

        cardMyPlan.setOnClickListener(this);
        cardTerms.setOnClickListener(this);
        cardPrivacy.setOnClickListener(this);
        cardRefund.setOnClickListener(this);
        cardAbout.setOnClickListener(this);
        cardContact.setOnClickListener(this);
        cardLogut.setOnClickListener(this);
    }

    private void Init(View view) {
        cardMyPlan = view.findViewById(R.id.cardMyPlan);
        cardTerms = view.findViewById(R.id.cardTerms);
        cardPrivacy = view.findViewById(R.id.cardPrivacy);
        cardRefund = view.findViewById(R.id.cardRefund);
        cardAbout = view.findViewById(R.id.cardAbout);
        cardContact = view.findViewById(R.id.cardContact);
        cardLogut = view.findViewById(R.id.cardLogout);

        txtProfileName = view.findViewById(R.id.txtProfileName);
        txtProfileMobile = view.findViewById(R.id.txtProfileMobile);
        mProfileCard = view.findViewById(R.id.profile_card);

        txtVersion = view.findViewById(R.id.txtVersion);
        txtVersion.setText("Version " + BuildConfig.VERSION_NAME);

        logoutProgress = view.findViewById(R.id.logoutProgress);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mProfileCard.getLayoutParams();
        layoutParams.topMargin = activity.getStatusBarHeight();
        mProfileCard.setLayoutParams(layoutParams);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    private void selfLogout() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSelfLogout(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                logoutProgress.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject data = jsonObject.getJSONObject("data");
                    String logout = data.optString("logout");

                    if (logout.equalsIgnoreCase("true")) {
                        SharePreferenceManager.clearDB();

                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity()).toBundle();
                        startActivity(intent, bundle);
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                logoutProgress.setVisibility(View.GONE);
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                logoutProgress.setVisibility(View.GONE);
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cardMyPlan:
                activity.addFragment(new MyPlanFragment());
                break;

            case R.id.cardTerms:
                Bundle bundle = new Bundle();
                bundle.putString("DATA", "https://aeongroup.in/terms");

                WebFragment webFragment = new WebFragment();
                webFragment.setArguments(bundle);
                activity.addFragment(webFragment);
                break;

            case R.id.cardRefund:
                Bundle bundle4 = new Bundle();
                bundle4.putString("DATA", "https://aeongroup.in/refund");

                WebFragment webFragmentRefund = new WebFragment();
                webFragmentRefund.setArguments(bundle4);
                activity.addFragment(webFragmentRefund);
                break;

            case R.id.cardPrivacy:
                Bundle bundle1 = new Bundle();
                bundle1.putString("DATA", "https://aeongroup.in/privacy");

                WebFragment webFragment1 = new WebFragment();
                webFragment1.setArguments(bundle1);
                activity.addFragment(webFragment1);
                break;

            case R.id.cardAbout:
                Bundle bundle2 = new Bundle();
                bundle2.putString("DATA", "https://aeongroup.in/about");

                WebFragment webFragment2 = new WebFragment();
                webFragment2.setArguments(bundle2);
                activity.addFragment(webFragment2);
                break;

            case R.id.cardContact:
                Bundle bundle3 = new Bundle();
                bundle3.putString("DATA", "https://aeongroup.in/contact");

                WebFragment webFragment3 = new WebFragment();
                webFragment3.setArguments(bundle3);
                activity.addFragment(webFragment3);
                break;

            case R.id.cardLogout:
                logoutProgress.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selfLogout();
                    }
                }, 700);
                break;
        }
    }
}