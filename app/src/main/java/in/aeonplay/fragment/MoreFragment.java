package in.aeonplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.PaginationCommanChildAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.Comman.CommanData;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MoreFragment extends BaseFragment {

    private RecyclerView mMoreRecyclerView;
    private RelativeLayout rootContainer;
    private Bundle bundle;
    private ArrayList<CommanDataList> commanDataModel;
    private String headerTitle;
    private GridLayoutManager gridLayoutManager;
    private PaginationCommanChildAdapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int categoryID;
    private ProgressbarManager progressbarManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_comman, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new PaginationCommanChildAdapter((MainActivity) getActivity());

        Init(view);
        setData();

//        InterstitialAds.ShowInterstitialAds((MasterActivity) getActivity());
    }

    private void setData() {

        bundle = getArguments();
        if (bundle != null) {
            commanDataModel = bundle.getParcelableArrayList(Constants.DATA);
            headerTitle = bundle.getString(Constants.TITLE);
            categoryID = bundle.getInt(Constants.CATEGORY_ID);
        }

        if (commanDataModel != null) {
            progressbarManager.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rootContainer.setVisibility(View.VISIBLE);
                    progressbarManager.dismiss();

                    String contentType = commanDataModel.get(0).getContentType();
                    if (contentType.equalsIgnoreCase("movie") ||
                            contentType.equalsIgnoreCase("show") ||
                            contentType.equalsIgnoreCase("original")) {
                        gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
                        mMoreRecyclerView.setLayoutManager(gridLayoutManager);

                        mMoreRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(bundle.getString(Constants.PAGE) + "/category/" + categoryID + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        adapter.addAll(commanDataModel);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            adapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        gridLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
                        mMoreRecyclerView.setLayoutManager(gridLayoutManager);

                        mMoreRecyclerView.addOnScrollListener(new PaginationScrollListener(1,
                                gridLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(bundle.getString(Constants.PAGE) + "/category/" + categoryID + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

                        adapter.addAll(commanDataModel);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            adapter.addLoadingFooter();
                        else isLastPage = true;
                    }
                }
            }, Constants.INTERVAL);

        } else {
            activity.showMessageToUser("No Data Found!");
        }
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        rootContainer = view.findViewById(R.id.rootContainer);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootContainer.getLayoutParams();
        layoutParams.topMargin = activity.getStatusBarHeight() + activity.getToolbarHeight();
        rootContainer.setLayoutParams(layoutParams);

        mMoreRecyclerView = view.findViewById(R.id.commanList);
        mMoreRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mMoreRecyclerView.addItemDecoration(new SpaceItemDecoration());
        mMoreRecyclerView.setAdapter(adapter);
    }

    public void loadNextPage(String nextPageUrl) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCommanPagination(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), nextPageUrl);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                rootContainer.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();

                if (!response.equals("{\"data\":[]}")) {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    CommanData commanModel = gson.fromJson(reader, CommanData.class);

                    if (commanModel.getData() != null) {
                        TOTAL_PAGES = currentPage + 1;

                        adapter.removeLoadingFooter();
                        isLoading = false;

                        adapter.addAll(commanModel.getData());
                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        currentPage = 0;
                        adapter.removeLoadingFooter();
                        isLoading = false;
                    }

                } else {
                    currentPage = 0;
                    adapter.removeLoadingFooter();
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public int getNextPageNumber(String nextPage) {
        return nextPage == null ? 0 : Integer.parseInt(nextPage.split("=")[1]);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
