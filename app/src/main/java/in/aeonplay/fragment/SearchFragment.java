package in.aeonplay.fragment;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.adapter.AdvanceSearchAdapter;
import in.aeonplay.adapter.SearchAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.comman.SpaceItemDecorationSearch;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class SearchFragment extends BaseFragment {

    private LinearLayout mSearchContainer, searchFound, searchNotFound;
    private RecyclerView mMovieRecyclerView;
    private SearchAdapter homeChildAdapter;
    private String selectedType = "title";
    private SearchView mSearchView;
    private ImageView mAdvanceFilter;
    private TextView txtHint, txtErrorMsg;
    private int REQUEST_VOICE = 13;
    private int genreSelectedItem, languageSelectedItem, otherSelectedItem;
    public static String mSearchText = "";
    private Thread apiThread = null;
    private ProgressBar mProgressBar;
    private String mQuery = "";
    private static final long SEARCH_DELAY_MS = 1000L;
    private final Handler mHandler = new Handler();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(false);

        Init(v);
        setData();
    }

    private final Runnable mDelayedLoad = new Runnable() {
        @Override
        public void run() {
            getSearchItems(mQuery);
        }
    };

    private void setData() {

        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();

        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadQuery(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mProgressBar.setVisibility(View.VISIBLE);
                if (query.isEmpty())
                    mProgressBar.setVisibility(View.INVISIBLE);

                loadQuery(query);
                return false;
            }
        });


//        ImageView clearButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
//        clearButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mSearchView.setQuery("", false);
//                searchFound.setVisibility(View.GONE);
//
//                mSearchView.clearFocus();
//                mProgressBar.setVisibility(View.INVISIBLE);
//            }
//        });

//        ImageView voiceButton = (ImageView) mSearchView.findViewById(R.id.search_voice_btn);
//        voiceButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                askSpeechInput();
//            }
//        });

        SearchView.SearchAutoComplete editText = mSearchView.findViewById(androidx.appcompat.R.id.search_src_text);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
                return true;
            }
        });

        mAdvanceFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity, R.style.AppBottomSheetDialogTheme);
                LayoutInflater dialogInflater = LayoutInflater.from(getActivity());
                View dialogView = dialogInflater.inflate(R.layout.dialog_list, null);
                bottomSheetDialog.setContentView(dialogView);

                bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        BottomSheetDialog dialogc = (BottomSheetDialog) dialog;
                        FrameLayout bottomSheet = dialogc.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                        BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });

                bottomSheetDialog.show();

                Button btnApplyFilter = dialogView.findViewById(R.id.btnApplyFilter);
                Button btnClearAll = dialogView.findViewById(R.id.btnClearAll);

                btnApplyFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                btnClearAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                RecyclerView genreList = (RecyclerView) dialogView.findViewById(R.id.genreList);
                RecyclerView languageList = (RecyclerView) dialogView.findViewById(R.id.languageList);
                RecyclerView advanceList = (RecyclerView) dialogView.findViewById(R.id.advanceList);
                RecyclerView yearList = (RecyclerView) dialogView.findViewById(R.id.yearList);

                LinearLayoutManager genreLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                genreList.setLayoutManager(genreLayoutManager);
                genreList.setItemAnimator(new DefaultItemAnimator());
                genreList.addItemDecoration(new SpaceItemDecoration());

                LinearLayoutManager languageLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                languageList.setLayoutManager(languageLayoutManager);
                languageList.setItemAnimator(new DefaultItemAnimator());
                languageList.addItemDecoration(new SpaceItemDecoration());

                LinearLayoutManager advanceeLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                advanceList.setLayoutManager(advanceeLayoutManager);
                advanceList.setItemAnimator(new DefaultItemAnimator());
                advanceList.addItemDecoration(new SpaceItemDecoration());

                LinearLayoutManager yearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                yearList.setLayoutManager(yearLayoutManager);
                yearList.setItemAnimator(new DefaultItemAnimator());
                yearList.addItemDecoration(new SpaceItemDecoration());

                // Genre ....
                ArrayList<String> genreLst = new ArrayList<>();
                genreLst.add("Action");
                genreLst.add("Comedy");
                genreLst.add("Drama");
                genreLst.add("Romance");
                genreLst.add("Science Fiction");
                genreLst.add("Thriller");
                genreLst.add("All");

                setAdvanceData(genreList, genreLst, 0);

                // Language ....
                ArrayList<String> languageLst = new ArrayList<>();
                languageLst.add("Arabic");
                languageLst.add("Bengali");
                languageLst.add("English");
                languageLst.add("Gujarati");
                languageLst.add("Hindi");
                languageLst.add("Kannada");
                languageLst.add("Malayalam");
                languageLst.add("Marathi");
                languageLst.add("Punjabi");
                languageLst.add("Sanskrit");
                languageLst.add("Tamil");
                languageLst.add("Telugu");
                languageLst.add("Urdu");
                languageLst.add("All");

                setAdvanceData(languageList, languageLst, 1);

                // Year ....
                ArrayList<String> yearLst = new ArrayList<>();
                yearLst.add("2022");
                yearLst.add("2021");
                yearLst.add("2020");
                yearLst.add("2019");
                yearLst.add("2018");
                yearLst.add("2017");
                yearLst.add("2016");
                yearLst.add("2015");
                yearLst.add("2014");
                yearLst.add("2013");
                yearLst.add("All");

                setAdvanceData(yearList, yearLst, 2);

                // Advance ....
                ArrayList<String> advanceLst = new ArrayList<>();
                advanceLst.add("Cast & Crew");
                advanceLst.add("Latest");
                advanceLst.add("All");

                setAdvanceData(advanceList, advanceLst, 2);
            }
        });
    }

    private void setAdvanceData(RecyclerView recyclerView, ArrayList<String> list, int mode) {
        AdvanceSearchAdapter advanceSearchAdapter = new AdvanceSearchAdapter((MasterActivity) getActivity(), list);
        recyclerView.setAdapter(advanceSearchAdapter);
        advanceSearchAdapter.notifyDataSetChanged();
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void getSearchItems(String query) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getSearch(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), query);

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                mProgressBar.setVisibility(View.INVISIBLE);

                try {
                    JSONObject responseDataObj = new JSONObject(response);
                    JSONObject responseObj = responseDataObj.getJSONObject("data");
                    Iterator keys = responseObj.keys();

                    List<CommanDataList> suggestionList = new ArrayList<>();
                    List<CommanDataList> relatedList = new ArrayList<>();
                    while (keys.hasNext()) {
                        String currentDynamicKey = (String) keys.next();

                        if (currentDynamicKey.equalsIgnoreCase("related_content")) {
                            Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                            }.getType();
                            relatedList = new Gson().fromJson(
                                    responseObj.getJSONArray(currentDynamicKey).toString(), listType);
                        } else {
                            Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                            }.getType();
                            suggestionList = new Gson().fromJson(
                                    responseObj.getJSONArray(currentDynamicKey).toString(), listType);
                        }
                    }

                    if (suggestionList.size() > 0) {
                        searchNotFound.setVisibility(View.GONE);
                        searchFound.setVisibility(View.VISIBLE);

                        homeChildAdapter = new SearchAdapter(activity, suggestionList);
                        mMovieRecyclerView.setAdapter(homeChildAdapter);
                        homeChildAdapter.notifyDataSetChanged();
                        mMovieRecyclerView.setNestedScrollingEnabled(false);

                    } else if (relatedList.size() > 0){
                        searchNotFound.setVisibility(View.GONE);
                        searchFound.setVisibility(View.VISIBLE);

                        homeChildAdapter = new SearchAdapter(activity, relatedList);
                        mMovieRecyclerView.setAdapter(homeChildAdapter);
                        homeChildAdapter.notifyDataSetChanged();
                        mMovieRecyclerView.setNestedScrollingEnabled(false);

                    } else {
                        searchFound.setVisibility(View.GONE);
                        searchNotFound.setVisibility(View.VISIBLE);

                        txtErrorMsg.setText(getString(R.string.no_search_results, query));
                    }

                } catch (IllegalStateException |
                        JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (
                        Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                mProgressBar.setVisibility(View.INVISIBLE);
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                mProgressBar.setVisibility(View.INVISIBLE);
                activity.showMessageToUser(error);
            }
        });
    }

    private void Init(View v) {
        mSearchContainer = v.findViewById(R.id.searchContainer);
        mSearchView = v.findViewById(R.id.searchView);
        mProgressBar = v.findViewById(R.id.progressBar);
        mAdvanceFilter = v.findViewById(R.id.imgAdvanceFilter);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mSearchContainer.getLayoutParams();
        layoutParams.topMargin = activity.getStatusBarHeight();
        mSearchContainer.setLayoutParams(layoutParams);

        txtHint = v.findViewById(R.id.txtHint);
        txtErrorMsg = v.findViewById(R.id.errorMsg);
        searchFound = v.findViewById(R.id.searchFound);
        searchNotFound = v.findViewById(R.id.searchNotFound);

        mMovieRecyclerView = v.findViewById(R.id.recycler_search);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mMovieRecyclerView.setLayoutManager(gridLayoutManager);
        mMovieRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mMovieRecyclerView.addItemDecoration(new SpaceItemDecorationSearch());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            String query = data.getStringExtra(SearchManager.QUERY);
            mProgressBar.setVisibility(View.VISIBLE);
            mSearchView.setQuery(query, false);
            loadQuery(query);
        }
    }

    private void loadQuery(String query) {
        mHandler.removeCallbacks(mDelayedLoad);
        if (!TextUtils.isEmpty(query) && !query.equals("nil")) {
            mQuery = query;
            mHandler.postDelayed(mDelayedLoad, SEARCH_DELAY_MS);
        }
    }

    @Override
    public void onPause() {
        mHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

}