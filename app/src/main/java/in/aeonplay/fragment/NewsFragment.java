package in.aeonplay.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.NewsAdapter;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.comman.SpaceItemDecoration;
import in.aeonplay.model.NewsModel.NewsData;
import in.aeonplay.model.NewsModel.NewsHeader;
import in.aeonplay.model.NewsModel.NewsModel;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class NewsFragment extends BaseFragment {

    private RecyclerView mMovieRecyclerView;
    private RelativeLayout rootContainer;
    private FrameLayout adContainer;
    private ProgressbarManager progressbarManager;
    private List<NewsHeader> headerAdapter;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {

        Init(v);
        setData();
    }

    private void setData() {
        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getNewsList();
            }
        }, Constants.INTERVAL);
    }

    public void getNewsList() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getNews(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                progressbarManager.dismiss();
                rootContainer.setVisibility(View.VISIBLE);

                try {
                    Gson gson = new Gson();
                    Reader reader = new StringReader(response);
                    final NewsModel newsModel = gson.fromJson(reader, NewsModel.class);

                    ArrayList<String> languageList = new ArrayList<>();
                    for (NewsData newsData : newsModel.getData()) {
                        languageList.add(newsData.getLanguage());
                    }

                    List<String> newLanguageList = removeDuplicates(languageList);
                    Log.d("newLanguageList: ", "onSuccess: " + newLanguageList + "");

                    headerAdapter = new ArrayList<>();
                    for (String language : newLanguageList) {

                        List<NewsData> mList = new ArrayList<>();
                        for (NewsData newsData : newsModel.getData()) {
                            if (language.equalsIgnoreCase(newsData.getLanguage())) {
                                mList.add(newsData);
                            }
                        }

                        headerAdapter.add(new NewsHeader(language, mList));
                    }

                    NewsAdapter newsAdapter = new NewsAdapter((MainActivity) getActivity(), headerAdapter);
                    mMovieRecyclerView.setAdapter(newsAdapter);
                    newsAdapter.notifyDataSetChanged();

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                Log.d("TAG_NEWS: ", "onFailure: " + error + " " + responseCode);
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                Log.d("TAG_NEWS: ", "onError: " + error);
                activity.showMessageToUser(error);
            }
        });
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T element : list) {
            if (!newList.contains(element)) {
                newList.add(element);
            }
        }

        return newList;
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());
        adContainer = view.findViewById(R.id.adContainer);
        rootContainer = view.findViewById(R.id.rootContainer);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rootContainer.getLayoutParams();
        layoutParams.topMargin = activity.getStatusBarHeight() + activity.getToolbarHeight();
        rootContainer.setLayoutParams(layoutParams);

        mMovieRecyclerView = view.findViewById(R.id.commanList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        mMovieRecyclerView.setLayoutManager(gridLayoutManager);
        mMovieRecyclerView.addItemDecoration(new SpaceItemDecoration());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }

}