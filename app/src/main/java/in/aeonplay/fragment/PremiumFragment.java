package in.aeonplay.fragment;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import in.aeonplay.R;
import in.aeonplay.comman.OverlapPagerTitleStrip;

public class PremiumFragment extends BaseFragment {

    private ViewPager viewPager;
    private OverlapPagerTitleStrip pagerTitleStrip;
    private FrameLayout rootContainer;
    private MyPagerAdapter myPagerAdapter;
    //private ShapeableImageView shapeableImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_premium, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Init(view);
        setData();
    }

    private void setData() {
        pagerTitleStrip.setGravity(Gravity.TOP);
        pagerTitleStrip.setNonPrimaryAlpha(0.5f);
        pagerTitleStrip.setTextSpacing(100);
        pagerTitleStrip.setAllTitleBold(true);
        pagerTitleStrip.setSelectedTitleColor(true);
        pagerTitleStrip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 35);

        viewPager.setId(View.generateViewId());
        myPagerAdapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);
    }

    private void Init(View view) {
//        shapeableImageView = view.findViewById(R.id.shapeableImageView);
//        shapeableImageView.setShapeAppearanceModel(shapeableImageView.getShapeAppearanceModel()
//                .toBuilder()
//                .setBottomLeftCornerSize(200)
//                .setBottomLeftCorner(CornerFamily.ROUNDED, 200)
//                .build());
//
//        Glide.with(getActivity())
//                .load(R.mipmap.ic_action_erosnow)
//                .transition(GenericTransitionOptions.with(R.anim.fade_in))
//                .into(shapeableImageView);

        rootContainer = view.findViewById(R.id.rootContainer);
        viewPager = view.findViewById(R.id.viewPager);

        pagerTitleStrip = view.findViewById(R.id.pagerTitleStrip);
        pagerTitleStrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == 0)
                    viewPager.setCurrentItem(1, true);
                else
                    viewPager.setCurrentItem(0, true);
            }
        });

        rootContainer.setPadding(0, activity.getStatusBarHeight() + activity.getToolbarHeight(), 0, 0);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return PremiumContentFragment.newInstance(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
//                case 0:
//                    return "UPCOMING";

                case 0:
                    return "MOVIES";

                case 1:
                    return "TV SHOWS";

                default:
                    throw new IllegalStateException("Unexpected value: " + position);
            }
        }

    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        activity.removeFragment(fragment);
    }
}
