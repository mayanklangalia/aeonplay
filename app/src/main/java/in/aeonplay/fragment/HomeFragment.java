package in.aeonplay.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MainActivity;
import in.aeonplay.adapter.HomeAdapter;
import in.aeonplay.adapter.PaginationScrollListener;
import in.aeonplay.comman.Constants;
import in.aeonplay.comman.ProgressbarManager;
import in.aeonplay.model.Comman.CommanDataList;
import in.aeonplay.model.Comman.CommanHeader;
import in.aeonplay.network.APIClient;
import in.aeonplay.network.APIInterface;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class HomeFragment extends BaseFragment {

    private RecyclerView mHomeListing;

    private ProgressbarManager progressbarManager;
    private LinearLayout rootContainer;
    private boolean doubleBackToExitPressedOnce;
    private LinearLayoutManager mLinearLayoutManager;
    private HomeAdapter homeAdapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int PAGE_START = 1;
    private int currentPage = PAGE_START;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        homeAdapter = new HomeAdapter((MainActivity) getActivity());
        setHasOptionsMenu(true);
        Init(view);
        setData();
    }

    private void setData() {
        mLinearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        mHomeListing.setLayoutManager(mLinearLayoutManager);

        progressbarManager.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDashboardList();
            }
        }, Constants.INTERVAL);
    }

    private void Init(View view) {
        progressbarManager = new ProgressbarManager(getActivity());

//        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        mHomeListing = view.findViewById(R.id.homeListing);
        mHomeListing.setItemAnimator(new DefaultItemAnimator());
        mHomeListing.setAdapter(homeAdapter);
        mHomeListing.setNestedScrollingEnabled(false);

        rootContainer = view.findViewById(R.id.rootContainer);
    }

    @Override
    public void onBack() {
        if (activity.mDrawerLayout.isDrawerOpen(GravityCompat.END))
            activity.mDrawerLayout.closeDrawers();
        else {
            if (doubleBackToExitPressedOnce) {
                getActivity().finish();
                System.exit(0);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            activity.showMessageToUser(activity.getResources().getString(R.string.press_again));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    public void getDashboardList() {
        homeAdapter.getMovies().clear();
        homeAdapter.notifyDataSetChanged();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getDashboardList(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS));

        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {
                rootContainer.setVisibility(View.VISIBLE);
                progressbarManager.dismiss();

                try {
                    JSONObject responseDataObj = new JSONObject(response);
                    JSONObject responseObj = responseDataObj.getJSONObject("data");
                    Iterator keys = responseObj.keys();

                    List<CommanHeader> headerAdapter = new ArrayList<>();
                    while (keys.hasNext()) {
                        String currentDynamicKey = (String) keys.next();

                        Gson gson = new Gson();
                        Reader reader = new StringReader(responseObj.getJSONArray(currentDynamicKey).toString());
                        Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                        }.getType();
                        ArrayList<CommanDataList> list = gson.fromJson(reader, listType);

//                        Collections.shuffle(list);
                        if (list.size() > 0)
                            headerAdapter.add(new CommanHeader(currentDynamicKey, BuildConfig.AUTHORIZE_URL + "api/version1.0/contents", list));
                    }

                    if (headerAdapter != null) {
                        headerAdapter.add(0, new CommanHeader(false, true, false));
                        headerAdapter.add(1, new CommanHeader(false, false, true));
                        headerAdapter.add(2, new CommanHeader(true, false, false));

                        mHomeListing.addOnScrollListener(new PaginationScrollListener(0,
                                mLinearLayoutManager) {
                            @Override
                            protected void loadMoreItems() {
                                if (currentPage != 0) {
                                    isLoading = true;
                                    currentPage += 1;
                                    loadNextPage(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents" + "?page=" + currentPage);
                                }
                            }

                            @Override
                            public int getTotalPageCount() {
                                return TOTAL_PAGES;
                            }

                            @Override
                            public boolean isLastPage() {
                                return isLastPage;
                            }

                            @Override
                            public boolean isLoading() {
                                return isLoading;
                            }
                        });

//                        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//                            @Override
//                            public void onScrollChanged() {
//                                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);
//                                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
//                                        .getScrollY()));
//
//                                if (diff == 0) {
//                                    if (currentPage != 0) {
//                                        isLoading = true;
//                                        currentPage += 1;
//                                        loadNextPage(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents" + "?page=" + currentPage);
//                                    }
//                                }
//                            }
//                        });

                        homeAdapter.addAll(headerAdapter);
                        TOTAL_PAGES = currentPage;

                        if (currentPage <= TOTAL_PAGES)
                            homeAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        activity.showMessageToUser("Data not found!");
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    public void loadNextPage(String nextPage) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> loginCall = apiInterface.getCommanPagination(
                SharePreferenceManager.getString(Constants.TOKEN_TYPE) + " " +
                        SharePreferenceManager.getString(Constants.TOKEN_ACCESS), nextPage);
        APIClient.callAPI(activity, loginCall, new APIClient.APICallback() {
            @Override
            public void onSuccess(String response) {

                try {
                    if (!response.equals("{\"data\":[]}")) {
                        JSONObject responseDataObj = new JSONObject(response);
                        JSONObject responseObj = responseDataObj.getJSONObject("data");
                        Iterator keys = responseObj.keys();

                        List<CommanHeader> headerAdapter = new ArrayList<>();
                        while (keys.hasNext()) {
                            String currentDynamicKey = (String) keys.next();

                            Gson gson = new Gson();
                            Reader reader = new StringReader(responseObj.getJSONArray(currentDynamicKey).toString());
                            Type listType = new TypeToken<ArrayList<CommanDataList>>() {
                            }.getType();
                            ArrayList<CommanDataList> list = gson.fromJson(reader, listType);

//                            Collections.shuffle(list);
                            if (list.size() > 0)
                                headerAdapter.add(new CommanHeader(currentDynamicKey, BuildConfig.AUTHORIZE_URL + "api/version1.0/contents", list));
                        }

                        TOTAL_PAGES = currentPage + 1;

                        homeAdapter.removeLoadingFooter();
                        isLoading = false;

                        homeAdapter.addAll(headerAdapter);
                        if (currentPage <= TOTAL_PAGES) homeAdapter.addLoadingFooter();
                        else isLastPage = true;

                    } else {
                        currentPage = 0;
                        homeAdapter.removeLoadingFooter();
                        isLoading = false;
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    activity.showMessageToUser(exception.getMessage().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error, int responseCode) {
                activity.showMessageToUser(error + " " + responseCode);
            }

            @Override
            public void onError(String error) {
                activity.showMessageToUser(error);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.action_main, menu);
    }

    @SuppressLint("WrongConstant")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu:
                if (activity.mDrawerLayout.isDrawerOpen(Gravity.END)) {
                    activity.mDrawerLayout.closeDrawers();
                } else {
                    activity.mDrawerLayout.openDrawer(Gravity.END);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
