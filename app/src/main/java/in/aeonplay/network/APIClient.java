package in.aeonplay.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLHandshakeException;

import in.aeonplay.BuildConfig;
import in.aeonplay.R;
import in.aeonplay.activity.MasterActivity;
import in.aeonplay.comman.ConnectivityReceiver;
import in.aeonplay.comman.Constants;
import in.aeonplay.model.Login.JWTModel;
import in.aeonplay.model.Login.TokenModel;
import in.aeonplay.preferences.SharePreferenceManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;
    private static String TAG = APIClient.class.getSimpleName();
    private static ArrayList<PendingAPICall> apiCallArrayList = new ArrayList<>();

    public static Retrofit getClient() {

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new MyOkHttpInterceptor());
        client.connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.AUTHORIZE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        return retrofit;
    }

    public static class MyOkHttpInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Log.e("TAG", "intercept: " + originalRequest.url().toString());

            String token_type = SharePreferenceManager.getString(Constants.TOKEN_TYPE);
            String token_access = SharePreferenceManager.getString(Constants.TOKEN_ACCESS);
            Log.e("TAG", "intercept: " + token_type + " " + token_access);

            if (!token_type.equalsIgnoreCase("") && !token_access.equalsIgnoreCase("")) {
                Request newRequest = originalRequest.newBuilder().header("Authorization", token_type + " " + token_access).build();
                return chain.proceed(newRequest);
            }

            return chain.proceed(originalRequest);
        }
    }

    public static String isTokenValid(MasterActivity context) {

        // If AccessToken and RefreshToken are stored in local preferences.
        Log.e(TAG, "isTokenValid: " + SharePreferenceManager.getString(Constants.TOKEN_ACCESS) + " " +
                SharePreferenceManager.getString(Constants.TOKEN_REFRESH));
        if (!SharePreferenceManager.getString(Constants.TOKEN_ACCESS).equalsIgnoreCase("") &&
                !SharePreferenceManager.getString(Constants.TOKEN_REFRESH).equalsIgnoreCase("")) {

            // Check Token is Valid or Expire. In Login AccessToken you will get
            // 'headers/payload/signature' take 'payload' from accessToken.

            Log.e(TAG, "Token_Type: " + SharePreferenceManager.getString(Constants.TOKEN_TYPE));
            Log.e(TAG, "Token_ExpiresIn: " + SharePreferenceManager.getInt(Constants.TOKEN_EXPIRES_IN));
            Log.e(TAG, "Access_Token: " + SharePreferenceManager.getString(Constants.TOKEN_ACCESS));
            Log.e(TAG, "Refresh_Token: " + SharePreferenceManager.getString(Constants.TOKEN_REFRESH));

            // Get payload ...
            String[] loadAccessToken = SharePreferenceManager.getString(Constants.TOKEN_ACCESS).split("[.]");
            String loadPayload = loadAccessToken[1];

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                // Base64 Decode payload
                String decodedPart = new String(Base64.getUrlDecoder().decode(loadPayload), StandardCharsets.UTF_8);

                Gson gson = new Gson();
                Reader reader = new StringReader(decodedPart);
                JWTModel jwtModel = gson.fromJson(reader, JWTModel.class);

                long millis = System.currentTimeMillis();
                long currentSeconds = millis / 1000;
                Log.e(TAG, "CurrentTimeInMilliSecond & Seconds: " + millis + " & " + currentSeconds);

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(millis);
                Log.e(TAG, "CurrentDateTime: " + formatter.format(calendar.getTime()));

                // Check token is expired or not ...
                if (currentSeconds >= jwtModel.getExp()) {
                    Log.e(TAG, "Token is: " + "Expired");

                    //Call API for update Token and after getting new token implement below code
                    getRefreshToken(context);
                    return "Failure";

                } else {
                    Log.e(TAG, "Token is: " + "Valid");
                    return "Success";

                }
            }

        }
        return "";
    }

    private static void getRefreshToken(MasterActivity masterActivity) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> apiCall = apiInterface.getRefreshToken(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                masterActivity.getString(R.string.refresh_token),
                SharePreferenceManager.getString(Constants.TOKEN_REFRESH));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                try {
                    if (response.isSuccessful()) {
                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(response.body().string());
                            TokenModel loginModel = gson.fromJson(reader, TokenModel.class);

                            SharePreferenceManager.save(Constants.TOKEN_TYPE, loginModel.getTokenType());
                            SharePreferenceManager.save(Constants.TOKEN_ACCESS, loginModel.getAccessToken());
                            SharePreferenceManager.save(Constants.TOKEN_REFRESH, loginModel.getRefreshToken());
                            SharePreferenceManager.save(Constants.TOKEN_EXPIRES_IN, loginModel.getExpiresIn());

                            for (PendingAPICall pendingAPICall : apiCallArrayList) {
                                callAPI(masterActivity, pendingAPICall.call, pendingAPICall.callBack);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("RefreshToken: ", response.message().toString());
                    }

                } catch (IllegalStateException | JsonSyntaxException exception) {
                    Log.e("RefreshToken: ", exception.getMessage().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("RefreshToken: ", t.getMessage().toString());
            }
        });
    }

    public static void callAPI(final MasterActivity activity, Call<ResponseBody> call, final APICallback apiCallback) {
        final long start = System.currentTimeMillis();
        Log.e(TAG, "APILog: " + call.request().url().toString() + " Start:" + start);
        if (ConnectivityReceiver.isConnected() == false) {
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.showNoInternetDialog();
                    }
                });

            }
            return;
        }
        String result = isTokenValid(activity);
        if (result.equals("Success") || result.equals("")) {
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    long end = System.currentTimeMillis();
                    String url = call.request().url().toString();
                    Log.e(TAG, "APILog: " + url + " End:" + end + " Duration:" + (end - start) + " in milliseconds " + response.code());
                    if (response.code() == 200) {
                        try {
                            String res = response.body().string();
                            Log.e(TAG, "onResponse: " + res);
                            if (apiCallback != null) {
                                apiCallback.onSuccess(res);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            String error = response.errorBody().string();
                            Log.e(TAG, "onResponseError: " + url + "\n" + error);
                            apiCallback.onFailure(error, response.code());
                            //                        ExceptionHandler.recordException(new Exception("Url: " + url + "\nResponse:" + error));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    String url = call.request().url().toString();
                    long end = System.currentTimeMillis();
                    Log.e(TAG, "APILog:onFailure::" + url + " End:" + end + " Duration:" + (end - start) + " in milliseconds");
                    if (t.getCause() != null && t.getCause() instanceof SSLHandshakeException) {
                        if (apiCallback != null) {
                            apiCallback.onError("SSL Handshake Exception");
                        }
                        return;
                    }
                    if (t instanceof ConnectException) {
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    activity.showNoInternetDialog();
                                }
                            });

                        }
                        return;
                    }
                    Log.e(TAG, url + "\nReason:" + t);
                    //                ExceptionHandler.recordException(new Exception("Url: " + url + "\nException:" + t + " Duration:" + (end - start) + " in milliseconds"));
                    t.printStackTrace();
                }
            });

        } else {
            final PendingAPICall pendingAPICall = new PendingAPICall();
            pendingAPICall.call = call;
            pendingAPICall.callBack = apiCallback;
            apiCallArrayList.add(pendingAPICall);
        }
    }

    public interface APICallback {
        void onSuccess(String response);

        void onFailure(String error, int responseCode);

        void onError(String error);
    }

    public static class PendingAPICall {
        public Call<ResponseBody> call;
        public APICallback callBack;
    }
}
