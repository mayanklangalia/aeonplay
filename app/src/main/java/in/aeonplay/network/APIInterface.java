package in.aeonplay.network;

import in.aeonplay.BuildConfig;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIInterface {

    String MOVIE_BIGPOSTER_BASEURL = "https://image.tmdb.org/t/p/w1280/";
    String MOVIE_SMALLPOSTER_BASEURL = "https://image.tmdb.org/t/p/w780";

    // For getting TMDB popular movies
    String API_DOMAIN = "https://api.themoviedb.org/3/";
    @GET(API_DOMAIN + "discover/movie?sort_by=popularity.desc&api_key=40516bf2c3fcc9dbf1f146d8a047061d")
    Call<ResponseBody> getTMDBMovies();

    // For getting TMDB movie trailer
    @GET(API_DOMAIN + "movie/{movie_id}/videos?api_key=40516bf2c3fcc9dbf1f146d8a047061d")
    Call<ResponseBody> getTrailer(
            @Path("movie_id") String movie_id);

    // For OAuth Token ...
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "oauth/token")
    @FormUrlEncoded
    Call<ResponseBody> postOAuthToken(
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("scope") String scope);

    // For Check user is found or not
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/can-login")
    @FormUrlEncoded
    Call<ResponseBody> postUserCanLogin(
            @Header("Authorization") String accessToken,
            @Field("mobile_no") String mobile_no);

    // For User login
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/login/mobile")
    @FormUrlEncoded
    Call<ResponseBody> postUserLogin(
            @Header("Authorization") String accessToken,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("otp") String otp,
            @Field("mobile_no") String mobile_no,
            @Field("username") String username);

    // For User registration
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/register/mobile")
    @FormUrlEncoded
    public Call<ResponseBody> postUserRegister(
            @Header("Authorization") String accessToken,
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("otp") String otp,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("mobile_no") String mobile_no,
            @Field("username") String username);

    // For Send/Resend SMS
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/send-otp")
    @FormUrlEncoded
    Call<ResponseBody> postUserOTP(
            @Header("Authorization") String accessToken,
            @Field("mobile_no") String mobile_no,
            @Field("action") String action);

    // For getting refresh token
    @Headers("Accept: application/json")
    @POST(BuildConfig.AUTHORIZE_URL + "oauth/token")
    @FormUrlEncoded
    Call<ResponseBody> getRefreshToken(
            @Field("client_id") String client_id,
            @Field("client_secret") String client_secret,
            @Field("grant_type") String grant_type,
            @Field("refresh_token") String refresh_token);

    // For self logout
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/logout?")
    Call<ResponseBody> getSelfLogout(
            @Header("Authorization") String accessToken);

    // For getting user info
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/profile?")
    Call<ResponseBody> getUserProfile(
            @Header("Authorization") String accessToken);

    // For update user operator
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/mobile?")
    Call<ResponseBody> getUpdtOperator(
            @Header("Authorization") String accessToken);

    // For getting dashboard data
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents?")
    Call<ResponseBody> getDashboardList(
            @Header("Authorization") String accessToken);

    // For getting aeon featured data
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/aeonplay/content")
    Call<ResponseBody> getAeonFeaturedList(
            @Header("Authorization") String accessToken);

    // For getting live channels data
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/aeonplay/coming-soon?")
    Call<ResponseBody> getAeonComingSoonList(
            @Header("Authorization") String accessToken);

    // For getting movie list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/movie?")
    Call<ResponseBody> getMovieList(
            @Header("Authorization") String accessToken,
            @Query("page") String page);

    // For getting live channels data
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/live-channel?")
    Call<ResponseBody> getLiveTV(
            @Header("Authorization") String accessToken);

    // For getting tv shows data
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/tvshow?")
    Call<ResponseBody> getTVShows(
            @Header("Authorization") String accessToken);

    // For getting episode list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/type/episode/{show_id}")
    Call<ResponseBody> getEpisode(
            @Header("Authorization") String accessToken,
            @Path("show_id") String show_id);

    // For getting episode list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros/episode?")
    Call<ResponseBody> getEROSEpisode(
            @Header("Authorization") String accessToken,
            @Query("show_id") String show_id);

    // For getting package list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/packageBygroup?")
    Call<ResponseBody> getPackageList(
            @Header("Authorization") String accessToken);

    // For getting search list
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/contents/search?")
    @FormUrlEncoded
    Call<ResponseBody> getSearch(
            @Header("Authorization") String accessToken,
            @Field("keyword") String keyword);

    // For getting subscription history list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/subscription?")
    Call<ResponseBody> getSubscriptionHistory(
            @Header("Authorization") String accessToken);

    // For getting subscription history list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/users/subscription?")
    Call<String> getSubscriptionHistoryList(
            @Header("Authorization") String accessToken);

    // For getting playback info
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros/playable?")
    Call<ResponseBody> getPlaybackInfo(
            @Header("Authorization") String accessToken,
            @Query("asset_type") String asset_type,
            @Query("asset_id") String asset_id);

    // For comman pagination
    @GET
    Call<ResponseBody> getCommanPagination(
            @Header("Authorization") String accessToken,
            @Url String pagination);

    // For getting eros movie list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros?asset_type=movie")
    Call<ResponseBody> getErosMovieList(
            @Header("Authorization") String accessToken,
            @Query("page") String page);

    // For getting eros tv shows list
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/eros?asset_type=original")
    Call<ResponseBody> getErosTVShowsList(
            @Header("Authorization") String accessToken,
            @Query("page") String page);

    // For create a new order - razorpay
    @POST(BuildConfig.AUTHORIZE_URL + "api/version1.0/orders/create/{packageId}")
    Call<ResponseBody> createOrder(
            @Header("Authorization") String accessToken,
            @Path("packageId") String packageId);

    // For get order - razorpay
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/orders/status/{packageId}")
    Call<ResponseBody> getRazorpayOrder(
            @Header("Authorization") String accessToken,
            @Path("packageId") String packageId);

    // For checkout - razorpay
    @GET(BuildConfig.AUTHORIZE_URL + "payment/razorpay/{paymentId}")
    Call<ResponseBody> confirmOrder(
            @Header("Authorization") String accessToken,
            @Path("packageId") String packageId);

    // For getting news
    @GET(BuildConfig.AUTHORIZE_URL + "api/version1.0/news")
    Call<ResponseBody> getNews(
            @Header("Authorization") String accessToken);
}
